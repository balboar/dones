# dones

Watcher for dones tracker. 

<table>
  <tr>
     <td>First Screen Page</td>
     <td>History tab</td>
     <td>Full screen</td>
  </tr>
  <tr>
    <td><img src="assets/images/image3.jpg" width=270 height=480></td>
    <td><img src="assets/images/image2.jpg" width=270 height=480></td>
    <td><img src="assets/images/image1.jpg" width=270 height=480></td>
  </tr>
 </table>

## Build apk

```bash
flutter build apk --release --dart-define-from-file config/config.json
```
