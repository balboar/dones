enum Sport {
  bike,

  run,

  walk,

  ebike
}

extension SportExtension on Sport {
  String get value {
    switch (this) {
      case Sport.bike:
        return "bike";
      case Sport.run:
        return "run";
      case Sport.ebike:
        return 'ebike';

      default:
        return "walk";
    }
  }

  String get name {
    switch (this) {
      case Sport.bike:
        return "Bicicleta";
      case Sport.run:
        return "Corrar";
      case Sport.ebike:
        return 'E-Bike';

      default:
        return "Caminar";
    }
  }
}

class SportHelper {
  static Sport asignar(String sport) {
    switch (sport) {
      case "bike":
        return Sport.bike;
      case "run":
        return Sport.run;
      case "walk":
        return Sport.walk;
      case "ebike":
        return Sport.ebike;
      default:
        return Sport.walk;
    }
  }
}
