import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();
  static const CardTheme _cardThemeSytle = CardTheme(
    elevation: 2,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20.0))),
  );

  static final ThemeData lightTheme = ThemeData(
    cardTheme: _cardThemeSytle,
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.lightGreen,
      brightness: Brightness.light,
    ),
  );

  static final ThemeData darkTheme = ThemeData(
    cardTheme: _cardThemeSytle,
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.lightGreen,
      brightness: Brightness.dark,
    ),
  );
}
