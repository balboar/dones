import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:gpx/gpx.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:path_provider/path_provider.dart';

import 'package:dones/database/database2.dart';
import 'package:dones/models/gps_model.dart';
import 'package:dones/models/ruta_model.dart';
import 'package:dones/models/track_point.dart';
import 'package:dones/networking/apiResponse.dart';
import 'package:dones/networking/repository.dart';
import 'package:dones/utils/constants.dart';
import 'package:uuid/uuid.dart';

class DonesController extends ChangeNotifier {
  bool parado = true;

  late String userId;

  late Repository _repository;
  Response? status;
  final List<GpsPoint> _posiciones = [];
  final List<LatLng> _listaCoordenadas = [];
  final List<LatLng> _listaCoordenadasRuta = [];
  List<GpsPoint> get posiciones => _posiciones;
  List<LatLng> get listaCoordenadas => _listaCoordenadas;
  List<LatLng> get listaCoordenadasRuta => _listaCoordenadasRuta;
  Ruta _rutaActual =
      Ruta(uuid: 'a', date: 0, sport: Sport.bike, finished: false);

  Duration? _duracion;
  String duracion = '00:00';
  Duration? _tiempoEnMovimiento;
  String tiempoEnMovimiento = '00:00';

  final Distance _distancia = const Distance();
  double _distanciaKm = 0;

  String distancia = '0';
  String velocidadMedia = '0';
  String velocidadMaxima = '0';
  double velocidadMaximaKmH = 0;
  String velocidadMaximaMinporKm = '';
  String elevacionMaxima = '0';
  String elevacionMinima = '0';
  String velocidad = '0';
  String elevacionGanada = '0';
  String elevacionPerdida = '0';
  String horaEstimadaDeLlegada = '00:00';

  Ruta get rutaActual => _rutaActual;
  String distanciaRuta = '0';
  Icon? infoIcon;

  List<TrackPoint> _listaElevacionRuta = [];
  List<TrackPoint> get listaElevacionRuta => _listaElevacionRuta;
  int relacionVelocidadElevacion = 10;

  DonesController() {
    _repository = Repository();
    userId = const Uuid().v4();
  }

  int _lastPositionTimestamp = 0;

  int get lastPositionTimestamp => _lastPositionTimestamp;

  Future<void> getLastPositionTimestamp() async {
    List<Map<String, dynamic>> data =
        await DB.query(GpsPoint.table, orderBy: 'date DESC', limit: 1);
    _getLastRouteDataFromDB();
    if (data.isNotEmpty) {
      var gpsPoint = GpsPoint.fromDBMap(data.first);
      _lastPositionTimestamp = gpsPoint.date.toInt();
    }
    Future.value();
  }

  LatLng get centroMapa => const LatLng(42.3382468, -7.8632696);

  Future<bool> addFromServer(List<dynamic> data) async {
    status = null;
    infoIcon = null;
    bool getDataFromDB = false;

    try {
      for (var rutaMap in data) {
        var ruta = Ruta.fromApiMap(rutaMap);
        if ((ruta.uuid != _rutaActual.uuid) ||
            ((ruta.finished != _rutaActual.finished) &&
                (ruta.uuid == _rutaActual.uuid))) {
          await DB.insert(Ruta.table, ruta.toMap());
          getDataFromDB = true;
        }
        if ((rutaMap['positions'].isEmpty) && (ruta != _rutaActual)) {
          return true;
        }
        if (rutaMap['positions'].isNotEmpty) {
          List<dynamic> positions = rutaMap["positions"];
          List<Map<String, dynamic>> pos = positions
              .map((posicion) => GpsPoint.fromMap(posicion).toMap())
              .toList();
          await DB.batchInsert(GpsPoint.table, pos);
          getDataFromDB = true;
        }
      }
      if (getDataFromDB) {
        await _getLastRouteDataFromDB();
      }
      notifyListeners();
      return true;
    } catch (e) {
      status = Response.error(e.toString());

      return false;
    }
  }

  Future<bool> borrarRuta(String uuid) async {
    await DB.deleteItem(Ruta.table, uuid);
    await _repository.deleteRuta(uuid);
    return Future.value(true);
  }

  Future<List<Ruta>> cargarRutasDeDB() async {
    List<Map<String, dynamic>> rutas =
        await DB.query(Ruta.table, orderBy: 'date DESC');

    return rutas.map((e) => Ruta.fromMap(e)).toList();
  }

  Future<bool> cargarDatosRutaDesdeBD(Ruta ruta) async {
    List<Map<String, dynamic>> data = await DB.query(GpsPoint.table,
        where: 'ruta_id=? ', whereArgs: [ruta.uuid], orderBy: 'date ASC');
    _rutaActual = ruta;
    for (var item in data) {
      var position = GpsPoint.fromDBMap(item);

      if (_posiciones.isNotEmpty) {
        var distancia =
            _calcularDistanciaEntreDosPuntos(_posiciones.last, position);
        distancia = distancia / 1000.0000;
        position.distanciaInicio = _posiciones.last.distanciaInicio + distancia;
      }
      _posiciones.add(position);
      _listaCoordenadas.add(position.posicion);
    }

    if (_posiciones.isNotEmpty) {
      _calcularDatos();
    }

    return true;
  }

  Future<bool> _getLastRouteDataFromDB() async {
    List<Map<String, dynamic>> rutas =
        await DB.query(Ruta.table, orderBy: 'date DESC', limit: 1);
    if (rutas.isNotEmpty) {
      var ruta = Ruta.fromMap(rutas.first);
      if (ruta.uuid != _rutaActual.uuid) {
        _posiciones.clear();
        _listaCoordenadas.clear();
        _listaCoordenadasRuta.clear();
        _listaElevacionRuta.clear();
      }
      if ((ruta.ruta != _rutaActual.ruta) && (ruta.ruta != null)) {
        _cargarRuta(ruta.ruta!);
      }

      _rutaActual = ruta;
      var lastPositionDate = 0.0;
      if (_posiciones.isNotEmpty) {
        lastPositionDate = _posiciones.last.date;
      } else if (ruta.date > 0) {
        lastPositionDate = ruta.date.toDouble();
      }

      List<Map<String, dynamic>> data = await DB.query(GpsPoint.table,
          where: 'ruta_id=? and date>?',
          whereArgs: [_rutaActual.uuid, lastPositionDate.toInt()],
          orderBy: 'date ASC');
      if (data.isEmpty) {
        lastPositionDate = _rutaActual.date.toDouble();
      } else {
        for (var item in data) {
          var position = GpsPoint.fromDBMap(item);
          if (_posiciones.isNotEmpty) {
            var distancia =
                _calcularDistanciaEntreDosPuntos(_posiciones.last, position);
            distancia = distancia / 1000.0000;
            position.distanciaInicio =
                _posiciones.last.distanciaInicio + distancia;
          }
          _posiciones.add(position);
          _listaCoordenadas.add(position.posicion);
        }
      }

      if (_posiciones.isNotEmpty) {
        _calcularDatos();
        notifyListeners();
      }
    }
    return true;
  }

  double _calcularDistanciaEntreDosPuntos(GpsPoint punto1, GpsPoint punto2) {
    var distancia = _distancia(LatLng(punto1.latitude, punto1.longitude),
        LatLng(punto2.latitude, punto2.longitude));
    return distancia;
  }

  Future<bool> _calcularDatos() {
    _calcularDuracion();
    distancia = _calcularDistancia(_posiciones);
    _calcularVelocidadMaxima();
    _calculaVelocidad();
    _calcularElevacionMaxima();
    _calcularElevacionMinima();
    _calcularElevacion();
    _calculaVelocidadMedia();
    _calcularRelacionVelocidadElevacion();
    _calcularHoraEstimadaDeLlegada();
    return Future.value(true);
  }

  void _calcularRelacionVelocidadElevacion() {
    if (_posiciones.isNotEmpty) {
      if (velocidadMaximaKmH > 0) {
        relacionVelocidadElevacion =
            ((double.parse(elevacionMaxima) - double.parse(elevacionMinima)) /
                    velocidadMaximaKmH)
                .ceil();
      }
    }
    if (relacionVelocidadElevacion == 0) {
      relacionVelocidadElevacion = 1;
    }
  }

  String _calcularDistancia(List<dynamic> listaPosiciones) {
    _distanciaKm = 0;
    for (var i = 0; i < listaPosiciones.length - 1; i++) {
      _distanciaKm += _distancia.as(
          LengthUnit.Millimeter,
          LatLng(listaPosiciones[i].posicion.latitude,
              listaPosiciones[i].posicion.longitude),
          LatLng(listaPosiciones[i + 1].posicion.latitude,
              listaPosiciones[i + 1].posicion.longitude));
    }

    return (_distanciaKm / 1000000).toStringAsFixed(2);
  }

  String _calcularDistanciaRuta(List<dynamic> listaPosiciones) {
    _distanciaKm = 0;
    for (var i = 0; i < listaPosiciones.length - 1; i++) {
      _distanciaKm += _distancia.as(
          LengthUnit.Millimeter,
          LatLng(listaPosiciones[i].latitude, listaPosiciones[i].longitude),
          LatLng(listaPosiciones[i + 1].latitude,
              listaPosiciones[i + 1].longitude));
    }

    return (_distanciaKm / 1000000).toStringAsFixed(2);
  }

  void _calcularHoraEstimadaDeLlegada() {
    if (_rutaActual.ruta != null && double.tryParse(distanciaRuta)! > 0) {
      double resto;
      var tiempo =
          (double.tryParse(distanciaRuta)! - double.tryParse(distancia)!) /
              double.tryParse(velocidadMedia)!;
      if (tiempo.isFinite && !tiempo.isNaN) {
        resto = tiempo - tiempo.toInt();
        var tiempoEstimadoLlegada =
            Duration(hours: tiempo.toInt(), minutes: (resto * 60).toInt());
        var llegadaEn = DateTime.now().add(tiempoEstimadoLlegada);
        horaEstimadaDeLlegada = DateFormat('kk:mm a').format(llegadaEn);
      }
    }
  }

  Duration _calcularTiempoEnMovimiento(
      List<GpsPoint> posiciones, double speedThreshold) {
    if (posiciones.isEmpty) return const Duration(seconds: 0);

    int totalDuration = 0;
    DateTime lastValidTime;
    lastValidTime =
        DateTime.fromMillisecondsSinceEpoch(posiciones.first.date.toInt());
    for (GpsPoint position in posiciones) {
      if (position.speed > speedThreshold) {
        totalDuration += lastValidTime
            .difference(
                DateTime.fromMillisecondsSinceEpoch(position.date.toInt()))
            .inMilliseconds
            .abs();
      }
      lastValidTime =
          DateTime.fromMillisecondsSinceEpoch(position.date.toInt());
    }
    return Duration(milliseconds: totalDuration);
  }

  void _calcularDuracion() {
    if (_posiciones.isNotEmpty) {
      _duracion =
          DateTime.fromMillisecondsSinceEpoch(_posiciones.last.date.toInt())
              .difference(DateTime.fromMillisecondsSinceEpoch(
                  _posiciones.first.date.toInt()));
      _tiempoEnMovimiento = _calcularTiempoEnMovimiento(_posiciones, 2);
    } else {
      _duracion = const Duration(seconds: 0);
      _tiempoEnMovimiento = _duracion;
    }
    duracion = _duracion.toString().split('.').first.padLeft(8, "0");
    tiempoEnMovimiento =
        _tiempoEnMovimiento.toString().split('.').first.padLeft(8, "0");
  }

  void _calcularVelocidadMaxima() {
    if (rutaActual.sport == Sport.run) {
      _maxSpeedMinPerKm();
    } else {
      _maxSpeedKmPerHour();
    }
  }

  void _maxSpeedKmPerHour() {
    if (_posiciones.length < 2) {
      velocidadMaxima = '0';
      velocidadMaximaKmH = 0;
    } else {
      velocidadMaximaKmH = _posiciones
          .reduce((GpsPoint currentPoint, GpsPoint nextPoint) =>
              currentPoint.speed > nextPoint.speed ? currentPoint : nextPoint)
          .speed;
      velocidadMaxima = velocidadMaximaKmH.toStringAsFixed(2);
    }
  }

  void _maxSpeedMinPerKm() {
    if (_posiciones.length < 2) {
      velocidadMaxima = '0';
      velocidadMaximaMinporKm = '0';
    } else {
      var maxSpeedMinPerKm = _posiciones
          .reduce((GpsPoint currentPoint, GpsPoint nextPoint) =>
              currentPoint.speed < nextPoint.speed && currentPoint.speed > 0
                  ? currentPoint
                  : nextPoint)
          .speed;

      var minutes = maxSpeedMinPerKm.round();
      var seconds = (60 * maxSpeedMinPerKm).remainder(60);
      velocidadMaxima =
          '${minutes.toString().padLeft(2, '0')}:${seconds.toInt().toString().padLeft(2, '0')}';
      velocidadMaximaMinporKm = velocidadMaxima;
    }
  }

  void _calcularElevacionMaxima() {
    if (_posiciones.isEmpty) {
      elevacionMaxima = '0';
    } else {
      elevacionMaxima = _posiciones
          .reduce((GpsPoint currentPoint, GpsPoint nextPoint) =>
              currentPoint.elevation > nextPoint.elevation
                  ? currentPoint
                  : nextPoint)
          .elevation
          .toStringAsFixed(0);
    }
  }

  void _calcularElevacionMinima() {
    if (_posiciones.isEmpty) {
      elevacionMinima = '0';
    } else {
      elevacionMinima = _posiciones
          .reduce((GpsPoint currentPoint, GpsPoint nextPoint) =>
              currentPoint.elevation < nextPoint.elevation &&
                      currentPoint.elevation > 0
                  ? currentPoint
                  : nextPoint)
          .elevation
          .toStringAsFixed(0);
    }
  }

  void _calculaVelocidad() {
    if (_posiciones.isNotEmpty) {
      velocidad = _posiciones.last.speed.toStringAsFixed(2);
      parado = _posiciones.last.speed < 2;
    }
  }

  void _calculaVelocidadMedia() {
    if (rutaActual.sport == Sport.run) {
      var avgSpeedMinPerKm = _duracion!.inSeconds == 0
          ? 0
          : ((_duracion!.inSeconds / _distanciaKm) * 1000000 / 60);
      var minutes = avgSpeedMinPerKm.round();
      var seconds = (60 * avgSpeedMinPerKm).remainder(60);
      velocidadMedia =
          '${minutes.toString().padLeft(2, '0')}:${seconds.toInt().toString().padLeft(2, '0')}';
    } else if (_duracion!.inSeconds == 0) {
      velocidadMedia = '0';
    } else {
      velocidadMedia =
          (((_distanciaKm / 1000000) * 3600) / _duracion!.inSeconds)
              .toStringAsFixed(2);
    }
  }

  void _calcularElevacion() {
    if (_posiciones.isNotEmpty) {
      double auxElevacionGanada = 0;
      double auxElevacionPerdida = 0;
      double startTripElevation = _posiciones
          .firstWhere((GpsPoint element) => element.elevation > 0, orElse: () {
        return _posiciones.first;
      }).elevation;
      double previousElevation = startTripElevation;
      double beforePreviousElevation = startTripElevation;
      double adjustment = startTripElevation;
      for (var element in _posiciones) {
        if (beforePreviousElevation < previousElevation &&
            previousElevation > element.elevation) {
          auxElevacionGanada =
              auxElevacionGanada + (previousElevation - adjustment);
          adjustment = previousElevation;
        } else if (beforePreviousElevation > previousElevation &&
            element.elevation > previousElevation) {
          auxElevacionPerdida =
              auxElevacionPerdida + (adjustment - previousElevation);
          adjustment = previousElevation;
        }
        beforePreviousElevation = previousElevation;
        previousElevation = element.elevation;
      }
      elevacionGanada = auxElevacionGanada.toStringAsFixed(0);
      elevacionPerdida = auxElevacionPerdida.toStringAsFixed(0);
    }
  }

  LatLngBounds calcularLimitesMapa(List<LatLng> coordenadas) {
    if (coordenadas.isNotEmpty) {
      double? x0, x1, y0, y1;
      for (LatLng coordenada in coordenadas) {
        if (x0 == null) {
          x0 = x1 = coordenada.latitude;
          y0 = y1 = coordenada.longitude;
        } else {
          if (coordenada.latitude > x1!) {
            x1 = coordenada.latitude;
          }
          if (coordenada.latitude < x0) {
            x0 = coordenada.latitude;
          }
          if (coordenada.longitude > y1!) {
            y1 = coordenada.longitude;
          }
          if (coordenada.longitude < y0!) {
            y0 = coordenada.longitude;
          }
        }
      }
      return LatLngBounds(LatLng(x1!, y1!), LatLng(x0!, y0!));
    } else {
      return LatLngBounds(
          LatLng(centroMapa.latitude, centroMapa.latitude + 0.5),
          LatLng(centroMapa.longitude, centroMapa.longitude + 0.5));
    }
  }

  Future<bool> _calcularDatosRuta(Gpx gpx) {
    _listaCoordenadasRuta.clear();
    _listaElevacionRuta.clear();
    for (var element in gpx.trks[0].trksegs[0].trkpts) {
      _listaCoordenadasRuta.add(LatLng(element.lat!, element.lon!));

      if (element.ele! > 0) {
        if (_listaElevacionRuta.isEmpty) {
          _listaElevacionRuta.add(TrackPoint(
              0, element.ele, 0, 0, LatLng(element.lat!, element.lon!)));
        } else {
          var distanciaM = _distancia.as(
              LengthUnit.Meter,
              LatLng(
                  _listaCoordenadasRuta[_listaCoordenadasRuta.length - 2]
                      .latitude,
                  _listaCoordenadasRuta[_listaCoordenadasRuta.length - 2]
                      .longitude),
              LatLng(element.lat!, element.lon!));
          _listaElevacionRuta.add(TrackPoint(
              _listaElevacionRuta.last.elapsedTime,
              element.ele,
              _listaElevacionRuta.last.elapsedDistance + distanciaM / 1000,
              0,
              LatLng(element.lat!, element.lon!)));
        }
      }
    }

    distanciaRuta = _calcularDistanciaRuta(_listaCoordenadasRuta);
    notifyListeners();
    return Future.value(true);
  }

  Future<Gpx?> getRouteFile(String? filename) async {
    Gpx? text;
    Directory appDocumentsDirectory =
        await getApplicationSupportDirectory(); // 1
    String appDocumentsPath = appDocumentsDirectory.path + '/routes'; // 2
    var file = File('$appDocumentsPath/$filename');

    try {
      var s = await file.readAsString();
      text = GpxReader().fromString(s);
    } catch (e) {
      debugPrint(e.toString());
    }
    return text;
  }

  void _cargarRuta(
    String file,
  ) async {
    Gpx? gpx;
    gpx = await getRouteFile(file);
    if (gpx == null) {
      Response<Map<String, dynamic>> response =
          await _repository.fetchDonesRoute(file);
      if (response.status == Status.ERROR) {
        debugPrint(response.message);
        status = Response.error(response.message);
      }
    }
    gpx ??= await getRouteFile(file);
    if (gpx != null) {
      await _calcularDatosRuta(gpx);
    }
  }
}
