import 'dart:async';
import 'dart:io';

import 'package:dones/controllers/dones_controller.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

enum ServerStatus { online, offline, connecting }

String url = const String.fromEnvironment('socketUrl');

class SocketController {
  static late Function(ServerStatus, String) callback;
  static late DonesController _donesControllerRepo;
  ServerStatus _status = ServerStatus.connecting;

  static final SocketController _singleton = SocketController._internal(
    (p0, message) {
      callback(p0, message);
      return p0;
    },
  );

  StreamController<String> streamController =
      StreamController.broadcast(sync: true);

  late WebSocket channel;
  bool connected = false;

  factory SocketController(
      {required Function(ServerStatus, String) onChageState,
      required DonesController donesControllerRepo}) {
    callback = onChageState;
    _donesControllerRepo = donesControllerRepo;
    return _singleton;
  }

  SocketController._internal(onChageState) {
    initWebSocketConnection();
  }

  initWebSocketConnection() async {
    debugPrint("conecting...");
    channel = await _connectWs();
    debugPrint("socket connection initializied");
    channel.done.then((dynamic _) => _onDisconnected());

    if (_status != ServerStatus.online) {
      callback(ServerStatus.online, '');
      _status = ServerStatus.online;
    }
    broadcastNotifications();
  }

  broadcastNotifications() {
    channel.listen((streamData) {
      streamController.add(streamData);
    }, onDone: () {
      debugPrint("conecting aborted");
      connected = false;
      if (_status != ServerStatus.offline) {
        callback(ServerStatus.offline, 'Conection aborted');
        _status = ServerStatus.offline;
      }
      initWebSocketConnection();
    }, onError: (e) {
      if (_status != ServerStatus.offline) {
        callback(ServerStatus.offline, 'Server error: $e');
        _status = ServerStatus.offline;
      }
      debugPrint('Server error: $e');
      connected = false;
      initWebSocketConnection();
    });
  }

  _connectWs() async {
    try {
      String wsUrl = '$url${_donesControllerRepo.lastPositionTimestamp}';
      return await WebSocket.connect(wsUrl);
    } catch (e) {
      debugPrint("Error! can not connect WS  $e");
      if (_status != ServerStatus.offline) {
        callback(ServerStatus.offline, "Error! can not connect WS  $e");
        _status = ServerStatus.offline;
      }
      await Future.delayed(const Duration(milliseconds: 10000));
      return await _connectWs();
    }
  }

  void _onDisconnected() {
    connected = false;

    if (_status != ServerStatus.offline) {
      callback(ServerStatus.offline, '');
      _status = ServerStatus.offline;
    }
    initWebSocketConnection();
  }
}
