import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DonesInfoIcon extends StatelessWidget {
  final String texto;
  const DonesInfoIcon({this.texto = '', super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 80,
      child: DonesIcon(texto),
    );
  }
}

class DonesIcon extends StatefulWidget {
  final String texto;
  const DonesIcon(this.texto, {super.key});

  @override
  State<DonesIcon> createState() => _DonesIconState();
}

class _DonesIconState extends State<DonesIcon> {
  final double _smallImageSize = 25;
  final double _normalImageSize = 35;
  double _imageSize = 35;
  String imageAsset = '';
  bool showTwoBikes = false;
  @override
  Widget build(BuildContext context) {
    widget.texto.isNotEmpty ? _imageSize = _smallImageSize : _normalImageSize;
    return Consumer<DonesController>(builder:
        (BuildContext context, DonesController controller, Widget? child) {
      if ((controller.rutaActual.finished) || controller.parado) {
        showTwoBikes = false;
        switch (controller.rutaActual.sport) {
          case Sport.bike:
          case Sport.ebike:
            imageAsset = 'assets/images/man-biking-stop.gif';
          default:
            imageAsset = 'assets/images/standing.gif';
        }
      } else {
        switch (controller.rutaActual.sport) {
          case Sport.bike:
            imageAsset = 'assets/images/man-biking.gif';

          case Sport.ebike:
            imageAsset = 'assets/images/bike-biking.gif';
            showTwoBikes = true;

          case Sport.run:
            imageAsset = 'assets/images/running.gif';
          case Sport.walk:
            imageAsset = 'assets/images/walking.gif';
        }
      }

      if (!showTwoBikes) {
        return Image.asset(
          alignment: Alignment.centerLeft,
          imageAsset,
          height: _imageSize,
          width: _imageSize,
        );
      } else {
        return Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Image.asset(
                imageAsset,
                height: _imageSize,
                width: _imageSize,
              ),
              Image.asset(
                'assets/images/wind.gif',
                height: _imageSize / 2,
                width: _imageSize / 2,
              )
            ]);
      }
    });
  }
}
