import 'package:dones/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserController extends ChangeNotifier {
  User? _authenticatedUser;
  bool _isAuthenticating = false;
  bool authenticated = false;
  String? _token;

  final PublishSubject<bool> _userSubject = PublishSubject();

  bool get isAuthenticating => _isAuthenticating;

  PublishSubject<bool> get userSubject => _userSubject;

  User? get user {
    return _authenticatedUser;
  }

  Future<Map<String, dynamic>> authenticate(
      String user, String password, String server) async {
    _isAuthenticating = true;
    bool hasError = true;
    String? message;
    String checkUser = const String.fromEnvironment('user');
    String checkpass = const String.fromEnvironment('password');

    if (user == checkUser && password == checkpass) {
      hasError = false;

      _authenticatedUser = User(user: user, token: password);

      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', password);
      prefs.setString('user', user);
      prefs.setString('server', server);
      authenticated = true;
      _userSubject.add(true);
    } else {
      message = 'Error iniciando sesión';
      _isAuthenticating = false;
    }
    notifyListeners();
    return {'success': !hasError, 'message': message};
  }

  void autoAuthenticate(BuildContext context) async {
    _isAuthenticating = true;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getString('token');
    if (_token != null) {
      final String? user = prefs.getString('user');
      _authenticatedUser = User(user: user, token: _token);
      _userSubject.add(true);
    } else {
      _isAuthenticating = false;
    }
    notifyListeners();
  }

  void logout() async {
    _userSubject.add(false);
    _authenticatedUser = null;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    prefs.remove('user');
    prefs.remove('server');
    _isAuthenticating = false;
    _userSubject.add(true);
  }
}
