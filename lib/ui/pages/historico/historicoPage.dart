import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/models/ruta_model.dart';
import 'package:dones/ui/pages/mainPage/components/map.dart';
import 'package:dones/ui/pages/trackDetailPage.dart/trackDetailPage.dart';
import 'package:dones/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';

class HistoricoPage extends StatelessWidget {
  const HistoricoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Histórico',
        ),
      ),
      body: const HistoricoPageBody(),
    );
  }
}

class HistoricoPageBody extends StatefulWidget {
  const HistoricoPageBody({super.key});

  @override
  _HistoricoPageBodyState createState() => _HistoricoPageBodyState();
}

class _HistoricoPageBodyState extends State<HistoricoPageBody> {
  final List<DonesController> _rutas =
      List<DonesController>.empty(growable: true);
  final List<Ruta> _rutasMap = List<Ruta>.empty(growable: true);
  @override
  void initState() {
    initializeDateFormatting('es_ES');
    initData();
    super.initState();
  }

  Future<void> initData() async {
    _rutasMap.addAll(await Provider.of<DonesController>(context, listen: false)
        .cargarRutasDeDB());

    for (var ruta in _rutasMap) {
      final controller = DonesController();

      _rutas.add(controller);
    }
    setState(() {});
  }

  Widget _buildContent() {
    return ListView.builder(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      itemCount: _rutas.length,
      itemBuilder: (BuildContext context, int index) {
        return Dismissible(
          direction: DismissDirection.endToStart,
          key: UniqueKey(),
          background: Container(
            color: Colors.red,
            child: const Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: Icon(
                  Icons.delete_outlined,
                  size: 30,
                ),
              ),
            ),
          ),
          confirmDismiss: (DismissDirection direction) {
            return _showMyDialog();
          },
          onDismissed: (DismissDirection direction) async {
            await Provider.of<DonesController>(context, listen: false)
                .borrarRuta(_rutas[index].rutaActual.uuid);
            setState(() {
              _rutas.removeAt(index);
            });
          },
          child: GpxCard(
            donesController: _rutas[index],
            ruta: _rutasMap[index],
            context: context,
          ),
        );
      },
    );
  }

  Future<bool?> _showMyDialog() async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: const Center(child: Text('Borrar ruta')),
          content: const Text(
              "Se va a borrar la ruta del servidor y del telefono. ¿Estas segura? Esta acción es irreversible"),
          actionsPadding: const EdgeInsets.symmetric(horizontal: 5.0),
          buttonPadding: const EdgeInsets.symmetric(horizontal: 15.0),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: const Text(
                'Borrar',
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: (() {
        initData();
        return Future.value();
      }),
      child: _rutas.isEmpty
          ? const Center(child: Text('Cargando...'))
          : _buildContent(),
    );
  }
}

class GpxCard extends StatefulWidget {
  const GpxCard(
      {super.key,
      required DonesController donesController,
      required Ruta ruta,
      required this.context})
      : _ruta = ruta,
        _donesController = donesController;

  final DonesController _donesController;
  final Ruta _ruta;
  final BuildContext context;

  @override
  State<GpxCard> createState() => _GpxCardState();
}

class _GpxCardState extends State<GpxCard> {
  @override
  void initState() {
    if (widget._donesController.listaCoordenadas.isEmpty) {
      widget._donesController.cargarDatosRutaDesdeBD(widget._ruta).then((_) {
        setState(() {});
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String _icon;
    switch (widget._donesController.rutaActual.sport) {
      case Sport.walk:
        _icon = '🚶';
        break;
      case Sport.run:
        _icon = '🏃';
        break;
      case Sport.ebike:
        _icon = '🚵‍♀️';
      default:
        _icon = '🚴';
        break;
    }
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                TrackDetailPage(widget._donesController)));
        widget._donesController;
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 0.0),
                child: Row(children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Text(
                      _icon,
                      style: const TextStyle(fontSize: 20),
                    ),
                  ),
                  Text(
                    widget._donesController.rutaActual.sport.name,
                    style: Theme.of(context).textTheme.titleMedium,
                  )
                ]),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    DataPoint(
                      icon: Icons.timer_outlined,
                      label: widget._donesController.duracion,
                    ),
                    DataPoint(
                        icon: Icons.map_outlined,
                        label: '${widget._donesController.distancia} km'),
                    DataPoint(
                      icon: Icons.show_chart_outlined,
                      label: '${widget._donesController.elevacionGanada} m',
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 140,
                child: widget._donesController.listaCoordenadas.isEmpty
                    ? Center(
                        child: Text(
                        'Sin datos',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ))
                    : DonesMiniMap(widget._donesController),
              ),
              const SizedBox(
                height: 8.0,
              ),
              Text(
                widget._donesController.posiciones.isNotEmpty
                    ? '${DateFormat.yMMMMEEEEd('es_ES').format(
                        DateTime.fromMillisecondsSinceEpoch(
                          widget._donesController.posiciones.first.date.toInt(),
                        ),
                      )} a las ${DateFormat.Hm('es_ES').format(
                        DateTime.fromMillisecondsSinceEpoch(
                          widget._donesController.posiciones.first.date.toInt(),
                        ),
                      )}'
                    : '',
                style: Theme.of(context).textTheme.bodySmall,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class DataPoint extends StatelessWidget {
  final IconData icon;
  final String label;
  const DataPoint({required this.icon, required this.label, super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          size: 14.0,
        ),
        const SizedBox(
          width: 4.0,
        ),
        Text(
          label,
        )
      ],
    );
  }
}
