import 'package:dones/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  final Map<String, dynamic> _formData = {
    'user': null,
    'password': null,
    'server': null
  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildEmailTextField() {
    return TextFormField(
      decoration: const InputDecoration(labelText: 'Usuario'),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Introduce un usuario';
        } else {
          return null;
        }
      },
      onSaved: (String? value) {
        _formData['user'] = value;
      },
    );
  }

  Widget _buildServerTextField() {
    return TextFormField(
      decoration: const InputDecoration(labelText: 'Servidor'),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Introduce la dirección del servidor';
        } else {
          return null;
        }
      },
      onSaved: (String? value) {
        _formData['server'] = value;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      decoration: const InputDecoration(labelText: 'Contraseña'),
      obscureText: true,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Introduce una contraseña';
        } else {
          return null;
        }
      },
      onSaved: (String? value) {
        _formData['password'] = value;
      },
    );
  }

  void _submitForm(Function authenticate) async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    _formKey.currentState!.save();
    Map<String, dynamic>? successInformation;
    successInformation = await authenticate(
        _formData['user'], _formData['password'], _formData['server']);
    if (successInformation!['success']) {
      Navigator.pushReplacementNamed(context, '/');
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('A ocurrido un error'),
            content: Text(successInformation!['message']),
            actions: <Widget>[
              TextButton(
                child: const Text('Aceptar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  Widget _buildLoggingScreen() {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            _buildServerTextField(),
            const SizedBox(
              height: 10.0,
            ),
            _buildEmailTextField(),
            const SizedBox(
              height: 10.0,
            ),
            _buildPasswordTextField(),
            const SizedBox(
              height: 10.0,
            ),
            Consumer<UserController>(
              builder:
                  (BuildContext context, UserController model, Widget? child) {
                return model.isAuthenticating
                    ? const CircularProgressIndicator()
                    : ElevatedButton(
                        child: const Text('Iniciar sesión'),
                        onPressed: () => _submitForm(model.authenticate),
                      );
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: _buildLoggingScreen(),
        ),
      ),
    );
  }
}
