import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/ui/pages/mainPage/components/datafields.dart';
import 'package:dones/ui/pages/mainPage/components/map.dart';
import 'package:dones/ui/widgets/elevation_hover_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:intl/intl.dart';

class TrackDetailPage extends StatefulWidget {
  DonesController model;
  TrackDetailPage(this.model, {super.key});

  @override
  _TrackDetailPagePageState createState() => _TrackDetailPagePageState();
}

class _TrackDetailPagePageState extends State<TrackDetailPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            widget.model.posiciones.isNotEmpty
                ? '${DateFormat.yMMMd('es_ES').format(
                    DateTime.fromMillisecondsSinceEpoch(
                      widget.model.posiciones.first.date.toInt(),
                    ),
                  )} a las ${DateFormat.Hm('es_ES').format(
                    DateTime.fromMillisecondsSinceEpoch(
                      widget.model.posiciones.first.date.toInt(),
                    ),
                  )}'
                : '',
          ),
        ),
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 1.0),
            children: [
              Column(
                children: [
                  DataFields(widget.model),
                  Card(
                    child: SizedBox(
                      height: 250,
                      child: DonesMap(MapController(), widget.model, false,
                          showWholeRoute: true),
                    ),
                  ),
                  SizedBox(
                    height: 350,
                    child: Card(
                      elevation: 2.0,
                      child: Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(
                                top: 5.0, left: 10.0, bottom: 15.0),
                            child: Row(
                              children: [
                                Icon(Icons.av_timer),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Text(
                                  'Velocidad y elevación',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: ElevationHoverChart(
                              context,
                              widget.model,
                              false,
                              showSpeedLine: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // SpeedVsDistanceChart(
                  //   widget.model.posiciones,
                  //   widget.model.relacionVelocidadElevacion,
                  // ),
                  // AltitudeVsDistanceChart(widget.model.posiciones,
                  //     listaPuntosRuta: widget.model.listaElevacionRuta),
                ],
              ),
            ],
          ),
        ));
  }
}
