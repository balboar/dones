import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/controllers/theme_controller.dart';
import 'package:dones/models/gps_model.dart';
import 'package:dones/ui/widgets/elevation_hover_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:provider/provider.dart';

import '../../widgets/location_marker/dones_marker.dart';

class MapPage extends StatefulWidget {
  final DonesController? model;
  final bool update;
  const MapPage(
    this.model,
    this.update, {
    Key? key,
  }) : super(key: key);

  @override
  MapPageState createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  GpsPoint? posicionMapa;
  // ElevationPoint? hoverPoint;
  MapController? mapController;
  static const _markerSize = 40.0;
  bool _chartVisible = true;

  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  @override
  void initState() {
    mapController = MapController();

    super.initState();
  }

  Widget buildMap(DonesController model) {
    return Column(
      children: [
        Expanded(
          child: FlutterMap(
            mapController: mapController,
            options: MapOptions(
              maxZoom: 25.0,
              initialCameraFit: CameraFit.coordinates(
                  coordinates: model.listaCoordenadas,
                  padding: const EdgeInsets.all(30.0)),
              initialCenter: model.centroMapa,
            ),
            children: [
              TileLayer(
                tileBuilder: Provider.of<ThemeService>(
                  context,
                ).isDarkModeOn
                    ? darkModeTileBuilder
                    : null,
                maxZoom: 25.0,
                tileProvider: const FMTCStore('mapStore').getTileProvider(),
                urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
                subdomains: const ['a', 'b', 'c'],
              ),
              PolylineLayer(
                polylines: [
                  Polyline(
                    strokeWidth: 3.0,
                    color: Theme.of(context).colorScheme.primary,
                    pattern: const StrokePattern.solid(),
                    points: model.listaCoordenadas,
                  ),
                  Polyline(
                    strokeWidth: 3.0,
                    color: Colors.pink,
                    pattern: const StrokePattern.dotted(),
                    points: model.listaCoordenadasRuta,
                  ),
                ],
              ),
              MarkerLayer(markers: [
                if (model.listaCoordenadas.isNotEmpty)
                  model.posiciones.last.battery < 20
                      ? Marker(
                          point: model.listaCoordenadas.last,
                          alignment: Alignment.topCenter,
                          width: _markerSize * 4,
                          height: _markerSize * 2,
                          child: DonesMarker(
                            message:
                                'Bateria: ${model.posiciones.last.battery}%',
                          ),
                        )
                      : Marker(
                          point: model.listaCoordenadas.last,
                          width: _markerSize,
                          height: _markerSize,
                          child: Image.asset('assets/images/bikelocation.png'),
                          alignment: Alignment.topCenter,
                        ),
                if (posicionMapa is GpsPoint)
                  Marker(
                    point: posicionMapa!.posicion,
                    width: 10,
                    height: 10,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.deepOrange,
                          borderRadius: BorderRadius.circular(12)),
                    ),
                  )
              ]),
              Align(
                alignment: Alignment.bottomRight,
                child: IconButton.filledTonal(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      setState(() {
                        _chartVisible = !_chartVisible;
                      });
                    },
                    icon: _chartVisible
                        ? const Icon(Icons.arrow_drop_down_rounded)
                        : const Icon(Icons.arrow_drop_up_rounded)),
              ),
            ],
          ),
        ),
        Visibility(
          visible: _chartVisible,
          child: SizedBox(
            width: double.maxFinite,
            height: MediaQuery.of(context).size.height * 0.35,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: Row(
                    children: [
                      const Icon(Icons.location_on),
                      Text(
                        ' Lat: ${posicionMapa != null ? posicionMapa!.posicion.latitude.toStringAsFixed(4) : '0'} ' +
                            ' Long: ${posicionMapa != null ? posicionMapa!.posicion.longitude.toStringAsFixed(4) : '0'}     ',
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                      const Icon(Icons.landscape_rounded),
                      Text(
                        ' ${posicionMapa != null ? posicionMapa!.elevation.toStringAsFixed(0) : '0'} m',
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: NotificationListener<InfoHoverNotification>(
                    onNotification: (InfoHoverNotification notification) {
                      setState(() {
                        posicionMapa = notification.position;
                      });

                      return true;
                    },
                    child: ElevationHoverChart(
                      context,
                      model,
                      widget.update,
                      showSpeedLine: true,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: widget.update
          ? Consumer<DonesController>(
              builder:
                  (BuildContext context, DonesController model, Widget? child) {
                return buildMap(model);
              },
            )
          : buildMap(widget.model!),
    );
  }
}
