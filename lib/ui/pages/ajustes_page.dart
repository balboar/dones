import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dones/controllers/theme_controller.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AjustesPage extends StatefulWidget {
  const AjustesPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _AjustesPageState();
  }
}

class _AjustesPageState extends State<AjustesPage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );
  bool? notificar = false;
  bool? admin = false;
  late FirebaseFirestore firestore;
  late FirebaseMessaging messaging;
  late SharedPreferences prefs;
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      firestore = FirebaseFirestore.instance;
      messaging = FirebaseMessaging.instance;
    }
    getSettings();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    if (Platform.isAndroid) {
      final PackageInfo info = await PackageInfo.fromPlatform();
      setState(() {
        _packageInfo = info;
      });
    }
  }

  void getSettings() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      notificar = prefs.getBool('notificar');
      admin = prefs.getBool('admin');
    });
  }

  Widget _buildContent(BuildContext context) {
    int tapped = 0;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTapUp: (TapUpDetails details) {
              tapped++;
              if (tapped == 7) {
                prefs.setBool('admin', true);
                setState(() {
                  admin = true;
                });
              }
            },
            child: const SettingsTitleWidget(
              title: 'Ajustes',
              icon: Icons.app_settings_alt_outlined,
              color: Colors.lightGreen,
            ),
          ),
          const MyDivider(),
          SwitchListTile(
              secondary: const Icon(Icons.notifications_active_outlined),
              title: const Text('Recibir notificaciones'),
              subtitle: const Text(
                  'Recibir notificación cuando se inicie o finalice una actividad'),
              value: notificar ?? false,
              onChanged: (bool value) {
                setState(() {
                  notificar = value;
                });
                prefs.setBool('notificar', notificar!);

                messaging.getToken().then((String? token) {
                  if (Platform.isAndroid) {
                    if (notificar!) {
                      firestore.collection('clientes').add({
                        'token': token,
                        'fecha': DateTime.now(),
                        'version': _packageInfo.version,
                      });
                    } else {
                      firestore
                          .collection('clientes')
                          .where('token', isEqualTo: token)
                          .get()
                          .then((QuerySnapshot document) {
                        firestore
                            .collection('clientes')
                            .doc(document.docs.first.id)
                            .delete();
                      });
                    }
                  }
                });
              }),
          SwitchListTile(
              secondary: const Icon(Icons.dark_mode),
              title: const Text('Tema'),
              value: Provider.of<ThemeService>(
                context,
              ).isDarkModeOn,
              onChanged: (_) {
                Provider.of<ThemeService>(context, listen: false).toggleTheme();
                prefs.setBool(
                    'themeModeDart',
                    Provider.of<ThemeService>(
                      context,
                      listen: false,
                    ).isDarkModeOn);
              }),
          const SizedBox(
            height: 15.0,
          ),
          buildAdminSettings(),
          const SettingsTitleWidget(
            title: 'Acerca de',
            icon: Icons.info_outline,
            color: Colors.orange,
          ),
          const MyDivider(),
          ListTile(
            leading: const Icon(Icons.app_registration),
            title: const Text('Versión'),
            subtitle: Text(_packageInfo.version),
          )
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: _buildContent(context),
          ),
        ),
      ),
    );
  }

  Widget buildAdminSettings() {
    return Visibility(
      visible: admin ?? false,
      child: const Column(
        children: [
          SettingsTitleWidget(
            title: 'Admin',
            icon: Icons.admin_panel_settings_outlined,
            color: Colors.lightBlue,
          ),
          MyDivider(),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }
}

class SettingsTitleWidget extends StatelessWidget {
  final String title;
  final Color color;
  final IconData icon;
  const SettingsTitleWidget(
      {super.key,
      required this.title,
      required this.color,
      required this.icon});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 17.0),
          child: Icon(
            icon,
            color: color,
            size: 25.0,
          ),
        ),
        Text(
          title,
          textAlign: TextAlign.start,
          style: const TextStyle(fontSize: 18.0),
        ),
      ],
    );
  }
}

class MyDivider extends StatelessWidget {
  const MyDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const Divider(
      // color: Theme.of(context).accentColor,
      thickness: 2.0,
    );
  }
}
