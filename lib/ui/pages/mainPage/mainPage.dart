import 'dart:async';
import 'dart:io';
import 'package:dones/ui/navigation.dart';
import 'package:dones/ui/pages/ajustes_page.dart';
import 'package:dones/ui/pages/historico/historicoPage.dart';
import 'package:dones/ui/pages/mainPage/trackingPage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

class Mainpage extends StatefulWidget {
  const Mainpage({super.key});

  @override
  _MainpageState createState() => _MainpageState();
}

class _MainpageState extends State<Mainpage> {
  int _selectedIndex = 0;

  @override
  void initState() {
    if (Platform.isAndroid) {
      FirebaseFirestore.instance.settings =
          const Settings(persistenceEnabled: false);
      FirebaseFirestore.instance.clearPersistence();
    }
    getFirebaseClientsForNotification();

    super.initState();
  }

  void getFirebaseClientsForNotification() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool notificar;

    notificar = prefs.getBool('notificar') ?? false;
    if (notificar && Platform.isAndroid) {
      FirebaseMessaging.instance.getToken().then(
        (String? token) {
          FirebaseFirestore.instance
              .collection('clientes')
              .where('token', isEqualTo: token)
              .get()
              .then(
            (QuerySnapshot document) {
              FirebaseFirestore.instance
                  .collection('clientes')
                  .doc(document.docs.first.id)
                  .update({'fecha': DateTime.now()});
            },
          );
        },
      );
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);

      FirebaseMessaging.onMessage.listen(
        (RemoteMessage message) {
          if (message.notification != null) {
            debugPrint(
                'Message also contained a notification: ${message.notification}');
          }
        },
      );
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: SafeArea(
        child: IndexedStack(
          index: _selectedIndex,
          children: const [
            TrackingPage(),
            HistoricoPage(),
            AjustesPage(),
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(
        selectedIndex: _selectedIndex,
        destinations: allDestinations,
        onDestinationSelected: _onItemTapped,
      ),
    );
  }
}
