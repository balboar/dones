// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'socket_cubit.dart';

sealed class SocketState {
  abstract Color color;
}

class SocketOffline extends SocketState {
  String message;
  @override
  Color color = Colors.red;
  SocketOffline({
    required this.message,
  });
}

final class SocketConnecting extends SocketState {
  @override
  Color color = Colors.orange;
}

final class SocketOnline extends SocketState {
  @override
  Color color = Colors.green;
}
