import 'dart:convert';

import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/controllers/socket_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'socket_state.dart';

class SocketCubit extends Cubit<SocketState> {
  final DonesController donesControllerRepo;

  SocketCubit({required this.donesControllerRepo})
      : super(SocketOffline(message: ''));

  void connect() async {
    emit(SocketConnecting());
    await donesControllerRepo.getLastPositionTimestamp();
    var socket = SocketController(
        onChageState: onChangeSocketState,
        donesControllerRepo: donesControllerRepo);
    socket.streamController.stream.listen((event) {
      debugPrint(event);
      donesControllerRepo.addFromServer(json.decode(event));
    });
  }

  void onChangeSocketState(ServerStatus status, String message) {
    switch (status) {
      case ServerStatus.online:
        emit(SocketOnline());

      case ServerStatus.offline:
        emit(SocketOffline(message: message));
      case ServerStatus.connecting:
        emit(SocketConnecting());
    }
  }
}
