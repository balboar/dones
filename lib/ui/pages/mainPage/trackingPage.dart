import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/ui/pages/mainPage/components/datafields.dart';
import 'package:dones/ui/pages/mainPage/components/info_bar.dart';
import 'package:dones/ui/pages/mainPage/components/map.dart';
import 'package:dones/ui/widgets/elevation_hover_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:provider/provider.dart';
import 'package:latlong2/latlong.dart';

class TrackingPage extends StatelessWidget {
  const TrackingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.symmetric(horizontal: 1.0),
      children: const [
        DonesInfoBar(),
        SizedBox(height: 10),
        _DataFieldsWidget(),
        _MapWidget(),
        SizedBox(height: 20),
        _SpeedChartWidget(),
      ],
    );
  }
}

class _DataFieldsWidget extends StatefulWidget {
  const _DataFieldsWidget();

  @override
  State<_DataFieldsWidget> createState() => _DataFieldsWidgetState();
}

class _DataFieldsWidgetState extends State<_DataFieldsWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<DonesController>(
      builder: (BuildContext context, DonesController model, Widget? child) {
        return DataFields(model);
      },
    );
  }
}

class _MapWidget extends StatefulWidget {
  const _MapWidget();

  @override
  State<_MapWidget> createState() => _MapWidgetState();
}

class _MapWidgetState extends State<_MapWidget>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  late MapController mapController;

  @override
  void initState() {
    mapController = MapController();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  void _animatedMapMove(LatLng destLocation, double destZoom) {
    final _latTween = Tween<double>(
        begin: mapController.camera.center.latitude,
        end: destLocation.latitude);
    final _lngTween = Tween<double>(
        begin: mapController.camera.center.longitude,
        end: destLocation.longitude);
    final _zoomTween = Tween<double>(
        begin: mapController.camera.zoom, end: mapController.camera.zoom);

    var controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    Animation<double> animation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)),
          _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed ||
          status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<DonesController>(
      builder: (BuildContext context, DonesController model, Widget? child) {
        if (model.posiciones.isNotEmpty) {
          _animatedMapMove(model.posiciones.last.posicion, 10);
        }
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.5,
            width: double.maxFinite,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15), // Image border
              child: DonesMap(
                mapController,
                model,
                true,
              ),
            ),
          ),
        );
      },
    );
  }
}

class _SpeedChartWidget extends StatefulWidget {
  const _SpeedChartWidget();

  @override
  State<_SpeedChartWidget> createState() => _SpeedChartWidgetState();
}

class _SpeedChartWidgetState extends State<_SpeedChartWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: Card(
        elevation: 2.0,
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
              child: Row(
                children: [
                  Icon(Icons.av_timer),
                  SizedBox(width: 5.0),
                  Text(
                    'Velocidad y elevación',
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Consumer<DonesController>(
                builder: (BuildContext context, DonesController model,
                    Widget? child) {
                  return model.posiciones.isEmpty
                      ? const Placeholder()
                      : ElevationHoverChart(
                          context,
                          model,
                          true,
                          showSpeedLine: true,
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
