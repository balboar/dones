import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/controllers/dones_activity_icon_widget.dart';
import 'package:dones/ui/pages/mainPage/components/errorButton.dart';
import 'package:dones/ui/pages/mainPage/components/socket_indicator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DonesInfoBar extends StatefulWidget {
  const DonesInfoBar({super.key});

  @override
  DonesInfoBarState createState() => DonesInfoBarState();
}

class DonesInfoBarState extends State<DonesInfoBar> {
  @override
  Widget build(BuildContext context) {
    return Consumer<DonesController>(
        builder: (BuildContext context, DonesController model, Widget? child) {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
        height: 48.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const DonesInfoIcon(),
            Expanded(
                flex: 2,
                child: Center(
                  child: Text(
                      '${model.status != null ? model.status?.message : ''}',
                      style: const TextStyle(fontSize: 12.0)),
                )),
            const Expanded(
                flex: 1,
                child: Align(
                    alignment: Alignment.centerRight, child: ErrorButton())),
            const Align(
                alignment: Alignment.centerRight, child: SocketIndicator())
          ],
        ),
      );
    });
  }
}
