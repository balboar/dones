import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

class DonesPopup extends StatefulWidget {
  final Marker marker;

  DonesPopup(this.marker, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DonesPopupState(marker);
}

class _DonesPopupState extends State<DonesPopup> {
  final Marker _marker;

  _DonesPopupState(this._marker);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Text(
                '🚴‍♂️',
                style: const TextStyle(fontSize: 30.0),
              ),
            ),
            _cardDescription(context),
          ],
        ),
      ),
    );
  }

  Widget _cardDescription(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        constraints: const BoxConstraints(minWidth: 100, maxWidth: 200),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Text(
              'Ubicación',
              overflow: TextOverflow.fade,
              softWrap: false,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14.0,
              ),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 4.0)),
            Text(
              'Latitud: ${_marker.point.latitude}',
              style: const TextStyle(fontSize: 12.0),
            ),
            Text(
              'Longitud: ${_marker.point.longitude}',
              style: const TextStyle(fontSize: 12.0),
            ),
          ],
        ),
      ),
    );
  }
}
