import 'package:dones/ui/pages/mainPage/cubit/socket_cubit.dart';
import 'package:dones/ui/widgets/info_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ErrorButton extends StatefulWidget {
  const ErrorButton({super.key});

  @override
  State<ErrorButton> createState() => _ErrorButtonState();
}

class _ErrorButtonState extends State<ErrorButton> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SocketCubit, SocketState>(
      builder: (context, state) {
        if ((state is SocketOffline) && (state.message.isNotEmpty)) {
          return IconButton(
              onPressed: () {
                showInfoDialog(context, state.message);
              },
              icon: const Icon(
                Icons.sync_problem_rounded,
                color: Colors.red,
              ));
        } else {
          return Container();
        }
      },
    );
  }
}
