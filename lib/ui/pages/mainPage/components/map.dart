import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/controllers/theme_controller.dart';
import 'package:dones/ui/pages/mainPage/components/popUp.dart';
import 'package:dones/ui/widgets/openMapPageButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';

class DonesMap extends StatefulWidget {
  final MapController mapController;
  DonesController model;
  bool? showWholeRoute = false;
  bool tracking;
  DonesMap(this.mapController, this.model, this.tracking,
      {this.showWholeRoute, super.key});

  @override
  _DonesMapState createState() => _DonesMapState();
}

class _DonesMapState extends State<DonesMap> with TickerProviderStateMixin {
  late DonesController model;
  static const _markerSize = 40.0;

  final PopupController _popupLayerController = PopupController();
  @override
  void initState() {
    model = widget.model;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        interactionOptions: const InteractionOptions(
            flags: InteractiveFlag.all & ~InteractiveFlag.drag),
        onTap: (_, LatLng pos) => _popupLayerController.hideAllPopups(),
        initialCameraFit: CameraFit.coordinates(
          coordinates: model.listaCoordenadas.isNotEmpty
              ? model.listaCoordenadas
              : const [
                  LatLng(42.3382468, -7.8632696),
                  LatLng(42.3482468, -7.8532696)
                ],
          padding: const EdgeInsets.all(30.0),
        ),
        initialZoom: 13,
      ),
      children: [
        TileLayer(
          tileBuilder: Provider.of<ThemeService>(
            context,
          ).isDarkModeOn
              ? darkModeTileBuilder
              : null,
          tileProvider: const FMTCStore('mapStore').getTileProvider(),
          urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
          userAgentPackageName: 'com.rodrigo.dones',
        ),
        PolylineLayer(polylines: [
          Polyline(
            strokeWidth: 3.0,
            color: Theme.of(context).colorScheme.primary,
            pattern: const StrokePattern.solid(),
            points: model.listaCoordenadas,
          ),
          Polyline(
            strokeWidth: 3.0,
            color: Colors.pink,
            pattern: const StrokePattern.dotted(),
            points: model.listaCoordenadasRuta,
          ),
        ]),
        PopupMarkerLayer(
          options: PopupMarkerLayerOptions(
            popupDisplayOptions: PopupDisplayOptions(
              builder: (BuildContext _, Marker marker) {
                return DonesPopup(marker);
              },
            ),
            markers: [
              if (model.listaCoordenadas.isNotEmpty)
                Marker(
                  point: model.listaCoordenadas.last,
                  width: _markerSize,
                  height: _markerSize,
                  child: Image.asset('assets/images/bikelocation.png'),
                  alignment: Alignment.topCenter,
                ),
            ],
            popupController: _popupLayerController,
          ),
        ),
        // const SimpleAttributionWidget(
        //   source: Text('OpenStreetMap contributors'),
        // ),
        Align(
          alignment: Alignment.topRight,
          child: FlutterMapExpandButton(
            model: model,
            update: widget.tracking,
          ),
        ),
      ],
    );
  }
}

class DonesMiniMap extends StatefulWidget {
  final DonesController model;
  const DonesMiniMap(this.model, {super.key});

  @override
  DonesMiniMapState createState() => DonesMiniMapState();
}

class DonesMiniMapState extends State<DonesMiniMap>
    with TickerProviderStateMixin {
  late DonesController model;
  @override
  void initState() {
    model = widget.model;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        interactionOptions:
            const InteractionOptions(flags: InteractiveFlag.none),
        initialCameraFit: CameraFit.coordinates(
            coordinates: model.listaCoordenadas,
            padding: const EdgeInsets.all(10.0)),
        initialCenter: model.posiciones.isNotEmpty
            ? model.listaCoordenadas.last
            : const LatLng(42.3382468, -7.8632696),
      ),
      children: [
        TileLayer(
          tileBuilder: Provider.of<ThemeService>(
            context,
          ).isDarkModeOn
              ? darkModeTileBuilder
              : null,
          tileProvider: const FMTCStore('mapStore').getTileProvider(),
          urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
          userAgentPackageName: 'com.rodrigo.dones',
        ),
        PolylineLayer(
          polylines: [
            Polyline(
                strokeWidth: 3.0,
                color: Theme.of(context).colorScheme.primary,
                pattern: const StrokePattern.solid(),
                points: model.listaCoordenadas),
          ],
        ),
      ],
    );
  }
}
