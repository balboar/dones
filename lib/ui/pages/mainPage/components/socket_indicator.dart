import 'package:dones/ui/pages/mainPage/cubit/socket_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SocketIndicator extends StatefulWidget {
  const SocketIndicator({super.key});

  @override
  State<SocketIndicator> createState() => _SocketIndicatorState();
}

class _SocketIndicatorState extends State<SocketIndicator> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 12,
      width: 12,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color:
              BlocProvider.of<SocketCubit>(context, listen: true).state.color),
    );
  }
}
