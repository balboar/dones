import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/ui/pages/mainPage/components/weatherCard.dart';
import 'package:dones/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:intl/intl.dart';

class DataFields extends StatefulWidget {
  final DonesController model;

  const DataFields(this.model, {super.key});

  @override
  DataFieldsState createState() => DataFieldsState();
}

class DataFieldsState extends State<DataFields> {
  Widget buildCard(
      {required String label,
      required String data,
      required bool showRigthBorder,
      required bool showBottomBorder}) {
    return Expanded(
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              style: showBottomBorder ? BorderStyle.solid : BorderStyle.none,
              color: Theme.of(context).colorScheme.outlineVariant,
              width: 1,
            ),
            right: BorderSide(
              style: showRigthBorder ? BorderStyle.solid : BorderStyle.none,
              color: Theme.of(context).colorScheme.outlineVariant,
              width: 1,
            ),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              data,
              style:
                  const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
            ),
            Text(
              label,
              style: const TextStyle(
                fontSize: 11.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildFieldsRow({required List<Widget> children}) {
    return Row(
      children: children,
    );
  }

  Widget _buildFieldsColum({required List<Widget> children}) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center, children: children);
  }

  @override
  Widget build(BuildContext context) {
    final rutaActualVisible = widget.model.rutaActual.ruta != null;
    final PageController controller = PageController(initialPage: 0);
    String speedUnit = 'km/h';
    if (widget.model.rutaActual.sport == Sport.run) speedUnit = 'min/km';
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        children: [
          SizedBox(
            height: 140,
            width: double.maxFinite,
            child: PageView(
                scrollDirection: Axis.horizontal,
                controller: controller,
                pageSnapping: true,
                children: <Widget>[
                  _buildFieldsColum(children: [
                    _buildFieldsRow(
                      children: [
                        buildCard(
                            label: 'Duración',
                            data: widget.model.tiempoEnMovimiento,
                            showBottomBorder: true,
                            showRigthBorder: true),
                        buildCard(
                            label: 'Distancia',
                            data: '${widget.model.distancia} km',
                            showBottomBorder: true,
                            showRigthBorder: true),
                        buildCard(
                            label: 'Vel. Media',
                            data: '${widget.model.velocidadMedia} $speedUnit',
                            showBottomBorder: true,
                            showRigthBorder: false),
                      ],
                    ),
                    _buildFieldsRow(
                      children: [
                        rutaActualVisible
                            ? buildCard(
                                label: 'Hora llegada',
                                data: widget.model.horaEstimadaDeLlegada,
                                showBottomBorder: false,
                                showRigthBorder: true)
                            : buildCard(
                                label: 'Vel. Maxima',
                                data:
                                    '${widget.model.velocidadMaxima} $speedUnit',
                                showBottomBorder: false,
                                showRigthBorder: true),
                        buildCard(
                            label: 'Última posición',
                            data: widget.model.posiciones.isNotEmpty
                                ? DateFormat('dd/MM HH:mm').format(
                                    DateTime.fromMillisecondsSinceEpoch(widget
                                        .model.posiciones.last.date
                                        .toInt()))
                                : '',
                            showBottomBorder: false,
                            showRigthBorder: true),
                        const WeatherCard(),
                      ],
                    ),
                  ]),
                  _buildFieldsColum(children: [
                    _buildFieldsRow(
                      children: [
                        buildCard(
                            label: 'Bateria',
                            data:
                                '${widget.model.posiciones.isNotEmpty ? widget.model.posiciones.last.battery : '0'}%',
                            showBottomBorder: true,
                            showRigthBorder: true),
                        buildCard(
                            label: 'Velocidad',
                            data: '${widget.model.velocidad} $speedUnit',
                            showBottomBorder: true,
                            showRigthBorder: true),
                        buildCard(
                            label: 'Ascenso',
                            data: '${widget.model.elevacionGanada} m',
                            showBottomBorder: true,
                            showRigthBorder: false),
                      ],
                    ),
                    _buildFieldsRow(
                      children: [
                        buildCard(
                            label: 'Altitud Max.',
                            data: '${widget.model.elevacionMaxima} m',
                            showBottomBorder: false,
                            showRigthBorder: true),
                        buildCard(
                            label: 'Altitud Min.',
                            data: '${widget.model.elevacionMinima} m',
                            showBottomBorder: false,
                            showRigthBorder: true),
                        buildCard(
                          label: 'Hora de incio',
                          data: widget.model.posiciones.isNotEmpty
                              ? DateFormat('HH:mm').format(
                                  DateTime.fromMillisecondsSinceEpoch(widget
                                      .model.posiciones.first.date
                                      .toInt()))
                              : '',
                          showBottomBorder: false,
                          showRigthBorder: false,
                        ),
                      ],
                    )
                  ]),
                ]),
          ),
          SmoothPageIndicator(
            controller: controller, // PageController
            count: 2,
            effect: WormEffect(
                dotHeight: 8,
                dotWidth: 8,
                activeDotColor: Theme.of(context)
                    .colorScheme
                    .primary), // your preferred effect
          ),
        ],
      ),
    );
  }
}
