import 'package:dones/controllers/dones_controller.dart';
import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:weather/weather.dart';

class WeatherIcon extends StatelessWidget {
  final String label;
  const WeatherIcon({super.key, required this.label});

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: const TextStyle(fontSize: 20),
    );
  }
}

class WeatherCard extends StatelessWidget {
  const WeatherCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const WeatherInfo();
  }
}

class WeatherInfo extends StatefulWidget {
  const WeatherInfo({super.key});

  @override
  WeatherInfoState createState() => WeatherInfoState();
}

class WeatherInfoState extends State<WeatherInfo>
    with AutomaticKeepAliveClientMixin {
  late LatLng _posicion;
  bool _iniciado = false;
  WeatherFactory wf = WeatherFactory("a30e27a37f0d8df5b8c45361433fd299",
      language: Language.SPANISH);

  Stream<Weather> infoWheatherStream() async* {
    while (true) {
      if (_iniciado) {
        await Future.delayed(const Duration(minutes: 1));
      } else {
        await Future.delayed(const Duration(seconds: 5));
      }
      var posiciones =
          Provider.of<DonesController>(context, listen: false).posiciones;

      if (posiciones.isNotEmpty) {
        _iniciado = true;
        _posicion = posiciones.last.posicion;

        var infoWeather = await wf.currentWeatherByLocation(
            _posicion.latitude, _posicion.longitude);

        yield infoWeather;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double? temp = 0;

    String? weatherIcon = '';
    String ciudad = '';
    Widget icono;
    List<String> iconos = [
      '🌞',
      '🌤️',
      '☁️',
      '🌧️',
      '🌦️',
      '🌩️',
      '☃️',
      '🌫️',
      '🌙'
    ];
    super.build(context);
    return StreamBuilder<Weather>(
        stream: infoWheatherStream(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            temp = snapshot.data!.tempFeelsLike!.celsius;
            temp ??= snapshot.data!.temperature!.celsius;

            weatherIcon = snapshot.data!.weatherIcon;
            ciudad = snapshot.data!.areaName ?? '';
            switch (weatherIcon) {
              case '01d':
                {
                  icono = WeatherIcon(label: iconos[0]);
                }
                break;
              case '01n':
                {
                  icono = WeatherIcon(label: iconos[8]);
                }
                break;

              case '02d':
                {
                  icono = WeatherIcon(label: iconos[1]);
                }
                break;
              case '03d':
                {
                  icono = WeatherIcon(label: iconos[2]);
                }
                break;

              case '04d':
                {
                  icono = WeatherIcon(label: iconos[2]);
                }
                break;
              case '09d':
                {
                  icono = WeatherIcon(label: iconos[3]);
                }
                break;

              case '10d':
                {
                  icono = WeatherIcon(label: iconos[4]);
                }
                break;

              case '11d':
                {
                  icono = WeatherIcon(label: iconos[5]);
                }
                break;
              case '13d':
                {
                  icono = WeatherIcon(label: iconos[6]);
                }
                break;

              case '50d':
                {
                  icono = WeatherIcon(label: iconos[7]);
                }
                break;

              default:
                {
                  icono = Image.network(
                    'http://openweathermap.org/img/wn/$weatherIcon@2x.png',
                  );
                }
                break;
            }

            return Expanded(
              child: SizedBox(
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    weatherIcon!.isNotEmpty
                        ? Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: SizedBox(
                              width: 25,
                              height: 25,
                              child: Center(child: icono),
                            ),
                          )
                        : const Text(''),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${temp!.toStringAsFixed(1)}ºC',
                            style: const TextStyle(
                                fontSize: 15.0, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            ciudad + 'aaaaaa',
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(fontSize: 11.0),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Expanded(
              child: Container(
                height: 50,
                alignment: Alignment.center,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      '😔 ',
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Text(
                      snapshot.error.toString(),
                      style: const TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return Expanded(
              child: Container(
                height: 50,
                alignment: Alignment.center,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
                child: const CircularProgressIndicator(),
              ),
            );
          } else {
            return Expanded(
              child: Container(
                height: 50,
                alignment: Alignment.center,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '😔 ',
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Text(
                      'Sin datos de tiempo',
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ),
            );
          }
        });
  }

  @override
  bool get wantKeepAlive => true;
}
