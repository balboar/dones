import 'package:flutter/material.dart';

void showInfoDialog(BuildContext context, String text) {
  showDialog(context: context, builder: (_) => InfoDialog(text));
}

class InfoDialog extends StatelessWidget {
  String text;
  InfoDialog(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      icon: Icon(
        Icons.error_outline,
        size: 30,
        color: Theme.of(context).colorScheme.secondary,
      ),
      title: const Text("Oooops!!"),
      content: Text(text),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Aceptar')),
      ],
    );
  }
}
