import 'package:dones/ui/widgets/location_marker/triangle.dart';
import 'package:flutter/material.dart';

class DonesMarker extends StatelessWidget {
  final String message;
  const DonesMarker({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.primary,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
            ),
          ),
        ),
        CustomPaint(
            painter: Triangle(
          Theme.of(context).colorScheme.primary,
        )),
      ],
    );
  }
}
