// import 'package:dones/controllers/dones_controller.dart';
// import 'package:dones/models/gps_model.dart';
// import 'package:fl_chart/fl_chart.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';

// class SpeedVsDistanceChart extends StatelessWidget {
//   SpeedVsDistanceChart();
//   List<FlSpot> spotsElevation = [];
//   List<FlSpot> spotsSpeed = [];
//   double _minSpeed = 8000;
//   double _maxSpeed = 0;
//   double _intervalo = 5;

//   List<Color> gradientColors = [
//     const Color(0xff23b6e6),
//     const Color(0xff02d39a),
//   ];

//   List<Color> gradientColorsSpeed = [
//     const Color(0xff2c274c),
//     const Color(0xff46426c),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       elevation: 2.0,
//       child: Column(
//         children: [
//           const Padding(
//             padding: EdgeInsets.only(top: 5.0, left: 10.0, bottom: 15.0),
//             child: Row(
//               children: [
//                 Icon(Icons.av_timer),
//                 SizedBox(
//                   width: 5.0,
//                 ),
//                 Text(
//                   'Velocidad',
//                   style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
//                 ),
//               ],
//             ),
//           ),
//           Container(
//             decoration: const BoxDecoration(
//               borderRadius: BorderRadius.all(
//                 Radius.circular(18),
//               ),
//             ),
//             margin: const EdgeInsets.symmetric(horizontal: 10),
//             height: 250,
//             child: Consumer<DonesController>(
//               builder:
//                   (BuildContext context, DonesController model, Widget? child) {
//                 return LineChart(mainData(model));
//               },
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   LineChartData mainData(DonesController model) {
//     spotsElevation.clear();
//     spotsSpeed.clear();
//     for (var element in model.posiciones) {
//       if (element.speed > _maxSpeed) _maxSpeed = element.speed;
//       if (element.speed < _minSpeed) _minSpeed = element.speed;
//       spotsElevation.add(FlSpot(element.distanciaInicio,
//           element.elevation / model.relacionVelocidadElevacion));
//       spotsSpeed.add(FlSpot(element.distanciaInicio, element.speed));
//     }

//     _intervalo = model.posiciones.isNotEmpty
//         ? (model.posiciones.last.distanciaInicio / 10).roundToDouble()
//         : _intervalo;
//     return LineChartData(
//       lineTouchData: LineTouchData(
//         touchTooltipData: LineTouchTooltipData(
//             tooltipBgColor: Colors.white,
//             tooltipMargin: 5,
//             getTooltipItems: (touchedSpots) {
//               return touchedSpots.map((LineBarSpot touchedSpot) {
//                 const textStyle = TextStyle(
//                   color: Colors.black,
//                   fontWeight: FontWeight.bold,
//                   fontSize: 12,
//                 );
//                 if (touchedSpot.barIndex == 0) {
//                   return LineTooltipItem(
//                       'Dist. ${touchedSpot.x.toStringAsFixed(2)} km\n Elev. ${(touchedSpot.y * model.relacionVelocidadElevacion).toStringAsFixed(0)} m',
//                       textStyle);
//                 } else {
//                   return LineTooltipItem(
//                       'Vel. ${touchedSpot.y.toStringAsFixed(2)} km/h',
//                       textStyle);
//                 }
//               }).toList();
//             }),
//         handleBuiltInTouches: true,
//       ),
//       gridData: FlGridData(
//         show: false,
//         drawVerticalLine: true,
//         getDrawingHorizontalLine: (value) {
//           return FlLine(
//             color: const Color(0xff37434d),
//             strokeWidth: 1,
//           );
//         },
//         getDrawingVerticalLine: (value) {
//           return FlLine(
//             color: const Color(0xff37434d),
//             strokeWidth: 1,
//           );
//         },
//       ),
//       titlesData: FlTitlesData(
//         show: true,
//         rightTitles: SideTitles(showTitles: false),
//         topTitles: SideTitles(showTitles: false),
//         bottomTitles: SideTitles(
//           showTitles: true,
//           reservedSize: 22,
//           getTextStyles: (context, value) =>
//               const TextStyle(color: Color(0xff68737d), fontSize: 12),
//           margin: 8,
//           interval: _intervalo == 0 ? 5 : _intervalo,
//         ),
//         leftTitles: SideTitles(
//           showTitles: true,
//           getTextStyles: (context, value) => const TextStyle(
//             color: Color(0xff67727d),
//             fontSize: 12,
//           ),
//           getTitles: (double value) {
//             return value.toStringAsFixed(0);
//           },
//           // interval: _maxSpeed == 0
//           //     ? 1
//           //     : (_maxSpeed / 6).floorToDouble() > 0
//           //         ? _maxSpeed == 0
//           //             ? 1
//           //             : (_maxSpeed / 6).floorToDouble()
//           //         : 1,
//           reservedSize: 25,
//           margin: 12,
//         ),
//       ),
//       borderData: FlBorderData(
//           show: false,
//           border: Border.all(color: const Color(0xff37434d), width: 1)),
//       minX: 0,
//       maxX: model.posiciones.isNotEmpty
//           ? model.posiciones.last.distanciaInicio
//           : 0,
//       minY: _minSpeed - (_minSpeed % 100),
//       maxY: _maxSpeed + 1, // para que se vea bien la leyenda y no se solape
//       lineBarsData: [
//         LineChartBarData(
//           spots: spotsElevation,
//           isCurved: true,
//           colors: gradientColors,
//           barWidth: 2,
//           isStrokeCapRound: true,
//           dotData: FlDotData(
//             show: false,
//           ),
//           belowBarData: BarAreaData(
//             show: true,
//             colors:
//                 gradientColors.map((color) => color.withOpacity(0.3)).toList(),
//           ),
//         ),
//         LineChartBarData(
//           spots: spotsSpeed,
//           isCurved: true,
//           colors: gradientColorsSpeed,
//           barWidth: 1,
//           isStrokeCapRound: true,
//           dotData: FlDotData(
//             show: false,
//           ),
//           belowBarData: BarAreaData(
//             show: false,
//             colors:
//                 gradientColors.map((color) => color.withOpacity(0.3)).toList(),
//           ),
//         ),
//       ],
//     );
//   }
// }
