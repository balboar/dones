import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/models/gps_model.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class ElevationHoverChart extends StatefulWidget {
  final DonesController model;
  BuildContext context;
  final bool showSpeedLine;
  final bool update;
  ElevationHoverChart(this.context, this.model, this.update,
      {super.key, this.showSpeedLine = false});

  @override
  _ElevationHoverChartState createState() => _ElevationHoverChartState();
}

class _ElevationHoverChartState extends State<ElevationHoverChart> {
  List<FlSpot> spots = [];
  List<FlSpot> spotsRuta = [];
  final List<FlSpot> _spotsSpeed = [];

  double _minAltitude = 8000;

  double _maxAltitude = 0;

  double _minSpeed = 8000;

  double maxX = 0;
  int _speedElevationRatio = 1;

  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  List<Color> gradientColorsRuta = [
    const Color.fromARGB(255, 236, 95, 142),
    const Color.fromARGB(255, 226, 171, 190),
  ];

  @override
  void initState() {
    super.initState();
    if (!widget.update) {
      initData();
    }
  }

  void initData() {
    spots.clear();
    spotsRuta.clear();
    _spotsSpeed.clear();
    if (widget.showSpeedLine) {
      _speedElevationRatio = widget.model.relacionVelocidadElevacion;
    }
    _minAltitude = double.tryParse(widget.model.elevacionMinima) ?? 0;
    _maxAltitude = double.tryParse(widget.model.elevacionMaxima) ?? 0;

    for (var element in widget.model.posiciones) {
      if (element.speed < _minSpeed) _minSpeed = element.speed;
      if (widget.showSpeedLine) {
        _spotsSpeed.add(
            FlSpot(element.distanciaInicio, element.speed * 10.toInt() / 10));
      }
    }
    for (var element in widget.model.posiciones) {
      spots.add(FlSpot(element.distanciaInicio,
          (element.elevation - _minAltitude) / _speedElevationRatio));
    }
    maxX = widget.model.listaElevacionRuta.isNotEmpty
        ? widget.model.listaElevacionRuta.last.elapsedDistance
        : widget.model.posiciones.isEmpty
            ? 0
            : widget.model.posiciones.last.distanciaInicio;

    if (widget.model.posiciones.isNotEmpty &&
        widget.model.listaElevacionRuta.isNotEmpty &&
        widget.model.listaElevacionRuta.length >
            widget.model.posiciones.length) {
      for (var element in widget.model.listaElevacionRuta) {
        if (element.altitude! > _maxAltitude) {
          _maxAltitude = element.altitude ?? 0;
        }
        if (element.altitude! < _minAltitude) {
          _minAltitude = element.altitude ?? 0;
        }
        spotsRuta.add(FlSpot(element.elapsedDistance,
            (element.altitude! - _minAltitude) / _speedElevationRatio));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 18.0),
      child: LineChart(
        mainData(),
      ),
    );
  }

  Widget getVerticalTitles(value, TitleMeta meta) {
    return const Padding(
      padding: EdgeInsets.all(4.0),
      child: Text(''),
    );
  }

  Widget getHorizontalTitles(value, TitleMeta meta) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Text('${meta.formattedValue}km'),
    );
  }

  Widget getElevationTitles(value, TitleMeta meta) {
    return Text((_minAltitude +
            ((double.tryParse(meta.formattedValue) ?? 0) *
                _speedElevationRatio))
        .toStringAsFixed(0));
  }

  LineChartData mainData() {
    if (widget.update) {
      initData();
    }
    return LineChartData(
      lineTouchData: LineTouchData(
        touchCallback: (_, LineTouchResponse? response) {
          GpsPoint posit;

          if (response != null) {
            if (response.lineBarSpots != null) {
              if (widget.model.posiciones.length <
                      response.lineBarSpots![0].spotIndex &&
                  widget.model.listaElevacionRuta.isNotEmpty) {
                posit = widget
                    .model.posiciones[response.lineBarSpots![0].spotIndex];
              } else {
                posit = widget
                    .model.posiciones[response.lineBarSpots![0].spotIndex];
              }

              InfoHoverNotification(posit).dispatch(context);
            }
          }
          return;
        },
        touchTooltipData: LineTouchTooltipData(
            getTooltipColor: (_) => Colors.white,
            tooltipMargin: 5,
            getTooltipItems: (touchedSpots) {
              return touchedSpots.map((LineBarSpot touchedSpot) {
                const textStyle = TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                );
                if (touchedSpot.barIndex == 1) {
                  return LineTooltipItem(
                      'Dist. ${touchedSpot.x.toStringAsFixed(2)} km\n Elev. ${(_minAltitude + (touchedSpot.y * _speedElevationRatio)).toStringAsFixed(0)} m',
                      textStyle);
                } else {
                  return LineTooltipItem(
                      'Vel. ${touchedSpot.y.toStringAsFixed(2)} km/h',
                      textStyle);
                }
              }).toList();
            }),
        handleBuiltInTouches: true,
        getTouchLineStart: (data, index) => 0,
      ),
      gridData: const FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
            getTitlesWidget: getVerticalTitles,
            reservedSize: 36,
          ),
        ),
        topTitles: const AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
          ),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            interval: maxX == 0 ? 5 : maxX / 4,
            showTitles: true,
            reservedSize: 36,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            // interval: (_maxAltitude - _minAltitude) == 0
            //     ? 50
            //     : (_maxAltitude - _minAltitude) / 4,
            getTitlesWidget: getElevationTitles,
            reservedSize: 36,
          ),
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      lineBarsData: [
        LineChartBarData(
          show: spotsRuta.isNotEmpty,
          spots: spotsRuta,
          isCurved: true,
          gradient: LinearGradient(colors: gradientColorsRuta),
          barWidth: 2,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
              show: true,
              gradient: LinearGradient(
                colors: gradientColorsRuta
                    .map((color) => color.withOpacity(0.2))
                    .toList(),
              )),
        ),
        LineChartBarData(
          show: spots.isNotEmpty,
          spots: spots,
          isCurved: true,
          color: Theme.of(context).colorScheme.secondary,
          barWidth: 2,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            color: Theme.of(context).colorScheme.secondary.withOpacity(0.3),
          ),
        ),
        LineChartBarData(
          show: _spotsSpeed.isNotEmpty,
          spots: _spotsSpeed,
          isCurved: true,
          color: Theme.of(context).colorScheme.primary,
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: false,
          ),
        ),
      ],
    );
  }
}

// /// [Notification] emitted when graph is hovered
class InfoHoverNotification extends Notification {
  /// Hovered point coordinates
  final GpsPoint position;

  InfoHoverNotification(this.position);
}
