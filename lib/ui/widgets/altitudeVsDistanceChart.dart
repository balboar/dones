// import 'package:fl_chart/fl_chart.dart';
// import 'package:flutter/material.dart';

// import 'package:dones/models/gps_model.dart';
// import 'package:dones/models/track_point.dart';

// class AltitudeVsDistanceChart extends StatefulWidget {
//   final List<GpsPoint> listAltitudes;
//   final List<TrackPoint>? listaPuntosRuta;
//   const AltitudeVsDistanceChart(
//     this.listAltitudes, {
//     this.listaPuntosRuta,
//     Key? key,
//   }) : super(key: key);

//   @override
//   _AltitudeVsDistanceChartState createState() =>
//       _AltitudeVsDistanceChartState();
// }

// class _AltitudeVsDistanceChartState extends State<AltitudeVsDistanceChart> {
//   List<FlSpot> spots = [];
//   List<FlSpot> spotsRuta = [];
//   double _minAltitude = 8000;

//   double _maxAltitude = 0;

//   double _intervalo = 5;
//   double maxX = 0;
//   List<Color> gradientColors = [
//     const Color(0xff23b6e6),
//     const Color(0xff02d39a),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     spots.clear();
//     spotsRuta.clear();

//     _minAltitude = 8000;

//     _maxAltitude = 0;

//     _intervalo = 5;
//     maxX;
//     return Card(
//       elevation: 2.0,
//       child: Column(
//         children: [
//           Padding(
//             child: Row(
//               children: const [
//                 Icon(Icons.landscape),
//                 SizedBox(
//                   width: 5.0,
//                 ),
//                 Text(
//                   'Elevacion',
//                   style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
//                 ),
//               ],
//             ),
//             padding: const EdgeInsets.only(top: 5.0, left: 10.0, bottom: 15.0),
//           ),
//           Container(
//             decoration: const BoxDecoration(
//               borderRadius: BorderRadius.all(
//                 Radius.circular(18),
//               ),
//             ),
//             margin: const EdgeInsets.symmetric(horizontal: 10),
//             height: 250,
//             child: LineChart(mainData()),
//           ),
//         ],
//       ),
//     );
//   }

//   LineChartData mainData() {
//     for (var element in widget.listAltitudes) {
//       if (element.elevation > _maxAltitude) _maxAltitude = element.elevation;
//       if (element.elevation < _minAltitude) _minAltitude = element.elevation;
//       spots.add(FlSpot(element.distanciaInicio, element.elevation));
//     }

//     _intervalo = widget.listAltitudes.isNotEmpty
//         ? (widget.listAltitudes.last.distanciaInicio / 10).roundToDouble()
//         : _intervalo;

//     maxX = widget.listAltitudes.isNotEmpty
//         ? widget.listAltitudes.last.distanciaInicio
//         : 0;
//     if (widget.listaPuntosRuta != null &&
//         widget.listaPuntosRuta!.isNotEmpty &&
//         widget.listaPuntosRuta!.length > widget.listAltitudes.length) {
//       for (var element in widget.listaPuntosRuta!) {
//         if (element.altitude! > _maxAltitude) _maxAltitude = element.altitude!;
//         if (element.altitude! < _minAltitude) _minAltitude = element.altitude!;
//         spotsRuta.add(FlSpot(element.elapsedDistance, element.altitude!));
//       }

//       _intervalo = widget.listaPuntosRuta!.isNotEmpty
//           ? (widget.listaPuntosRuta!.last.elapsedDistance / 10).roundToDouble()
//           : _intervalo;
//       maxX = widget.listaPuntosRuta!.isNotEmpty
//           ? widget.listaPuntosRuta!.last.elapsedDistance
//           : 0;
//     }

//     return LineChartData(
//       lineTouchData: LineTouchData(
//         touchTooltipData: LineTouchTooltipData(
//             tooltipBgColor: Theme.of(context).scaffoldBackgroundColor,
//             getTooltipItems: (touchedSpots) {
//               return touchedSpots.map((LineBarSpot touchedSpot) {
//                 const textStyle = TextStyle(
//                   color: Colors.black,
//                   fontWeight: FontWeight.bold,
//                   fontSize: 12,
//                 );
//                 return LineTooltipItem(
//                     'Dist. ${touchedSpot.x.toStringAsFixed(2)} km\n Elev. ${touchedSpot.y.toStringAsFixed(0)} m',
//                     textStyle);
//               }).toList();
//             }),
//         handleBuiltInTouches: true,
//       ),
//       gridData: FlGridData(
//         show: false,
//         drawVerticalLine: true,
//         getDrawingHorizontalLine: (value) {
//           return FlLine(
//             color: const Color(0xff37434d),
//             strokeWidth: 1,
//           );
//         },
//         getDrawingVerticalLine: (value) {
//           return FlLine(
//             color: const Color(0xff37434d),
//             strokeWidth: 1,
//           );
//         },
//       ),
//       titlesData: FlTitlesData(
//         show: true,
//         topTitles: SideTitles(showTitles: false),
//         rightTitles: SideTitles(showTitles: false),
//         bottomTitles: SideTitles(
//           showTitles: true,
//           reservedSize: 22,
//           getTextStyles: (context, value) =>
//               const TextStyle(color: Color(0xff68737d), fontSize: 12),
//           margin: 8,
//           interval: _intervalo == 0 ? 5 : _intervalo,
//         ),
//         leftTitles: SideTitles(
//           getTitles: (double value) {
//             return value.toStringAsFixed(0);
//           },
//           showTitles: true,
//           getTextStyles: (context, value) => const TextStyle(
//             color: Color(0xff67727d),
//             fontSize: 12,
//           ),
//           //interval: interval,
//           reservedSize: 30,
//           margin: 12,
//         ),
//       ),
//       borderData: FlBorderData(
//           show: false,
//           border: Border.all(color: const Color(0xff37434d), width: 1)),
//       minX: 0,
//       maxX: maxX,
//       minY: _minAltitude - (_minAltitude % 100),
//       maxY: _maxAltitude,
//       lineBarsData: [
//         LineChartBarData(
//           spots: spots,
//           isCurved: true,
//           colors: gradientColors,
//           barWidth: 2,
//           isStrokeCapRound: true,
//           dotData: FlDotData(
//             show: false,
//           ),
//           belowBarData: BarAreaData(
//             show: true,
//             colors:
//                 gradientColors.map((color) => color.withOpacity(0.3)).toList(),
//           ),
//         ),
//         LineChartBarData(
//           spots: spotsRuta,
//           isCurved: true,
//           //colors:  //gradientColorsRuta as List<Color>?,
//           barWidth: 2,
//           isStrokeCapRound: true,
//           dotData: FlDotData(
//             show: false,
//           ),
//           belowBarData: BarAreaData(
//             show: true,
//             // colors: gradientColorsRuta
//             //     .map((color) => color!.withOpacity(0.2))
//             //     .toList(),
//           ),
//         ),
//       ],
//     );
//   }
// }
