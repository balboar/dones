import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/ui/pages/mapPage/mapPage.dart';
import 'package:flutter/material.dart';

class FlutterMapExpandButton extends StatelessWidget {
  final bool mini;
  final double padding;
  final Alignment alignment;
  final DonesController model;
  final bool update;

  // final FitBoundsOptions options =
  //     const FitBoundsOptions(padding: EdgeInsets.all(12));

  const FlutterMapExpandButton(
      {super.key,
      this.mini = true,
      this.padding = 4.0,
      this.alignment = Alignment.topRight,
      required this.update,
      required this.model});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: alignment,
      child: Padding(
        padding: EdgeInsets.only(left: padding, top: padding, right: padding),
        child: FloatingActionButton(
          heroTag: 'expandMap',
          mini: mini,
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => MapPage(model, update),
              ),
            );
          },
          child: const Icon(
            Icons.expand_rounded,
          ),
        ),
      ),
    );
  }
}
