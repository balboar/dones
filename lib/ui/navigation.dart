import 'package:flutter/material.dart';

const List<NavigationDestination> allDestinations = <NavigationDestination>[
  NavigationDestination(label: 'Seguimiento', icon: Icon(Icons.location_on)),
  NavigationDestination(label: 'Histórico', icon: Icon(Icons.history)),
  NavigationDestination(label: 'Ajustes', icon: Icon(Icons.settings_outlined)),
];
