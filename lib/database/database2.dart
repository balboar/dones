import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;

class Helper {
  final String path;

  Helper(this.path);

  Database? _db;

  static int get _version => 1;
  final _lock = Lock();

  static void _onCreate(Database db, int version) async {
    await db.transaction((txn) async {
      await txn.execute(
          'CREATE TABLE rutas (uuid TEXT PRIMARY KEY NOT NULL,date INTEGER NOT NULL, sport TEXT NOT NULL,ruta TEXT,finished INTEGER)');

      await txn.execute(
        'CREATE TABLE posiciones (id INTEGER PRIMARY KEY, ruta_id TEXT, date INTEGER,accuracy REAL, latitude REAL, longitude REAL, elevation REAL, speed REAL,heading REAL, battery INTEGER,FOREIGN KEY(ruta_id) REFERENCES rutas(uuid)  ON DELETE CASCADE)',
      );
      await txn.execute('CREATE INDEX ix_posiciones_date ON posiciones (date)');

      await txn
          .execute('CREATE INDEX ix_posiciones_ruta ON posiciones (ruta_id)');

      await txn.execute('CREATE INDEX ix_rutas_uuis ON rutas (uuid)');
    });
  }

  static void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    // Database version is updated, alter the table
    await db.transaction((txn) async {
      await txn.execute(
        '',
      );
    });
  }

  _onConfigure(Database db) async {
    // Add support for cascade delete
    await db.execute("PRAGMA foreign_keys = ON");
  }

  Future<Database?> getDb() async {
    if (_db == null) {
      await _lock.synchronized(() async {
        // Check again once entering the synchronized block
        _db ??= await openDatabase(path,
            version: _version,
            onCreate: _onCreate,
            onUpgrade: _onUpgrade,
            onConfigure: _onConfigure);
      });
    }
    return _db;
  }
}

class DB {
  static Database? _db;
  static String dbName = 'dones.db';

  static Future<void> init() async {
    if (_db != null) {
      return;
    }

    try {
      String path = '';
      if (Platform.isAndroid) {
        path = p.join(await (getDatabasesPath()), dbName);
      } else {
        Directory dir = await (getApplicationDocumentsDirectory());
        path = p.join(dir.path, dbName);
      }
      _db = await Helper(path).getDb();
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static Future<List<Map<String, dynamic>>> query(String table,
      {bool? distinct,
      List<String>? columns,
      String? where,
      List<Object?>? whereArgs,
      String? groupBy,
      String? having,
      String? orderBy,
      int? limit,
      int? offset}) async {
    return await _db!.transaction(
      (txn) async {
        // if (where != null && whereArgs != null)
        return await txn.query(table,
            where: where, whereArgs: whereArgs, orderBy: orderBy, limit: limit);
        //  else
        //     return await txn.query(table);
      },
    );
  }

  static Future<int> insert(String table, Map<String, dynamic> item) async {
    return await _db!.transaction((txn) async {
      return await txn.insert(table, item,
          conflictAlgorithm: ConflictAlgorithm.ignore);
    });
  }

  static Future<int> batchInsert(
      String table, List<Map<String, dynamic>> items) async {
    return await _db!.transaction((txn) async {
      var batch = txn.batch();
      for (var item in items) {
        batch.insert(table, item, conflictAlgorithm: ConflictAlgorithm.replace);
      }
      await batch.commit(noResult: true);
      return Future.value(0);
    });
  }

  static Future<int> deleteItem(String table, String uuid) async {
    return await _db!.delete(table, where: "uuid=?", whereArgs: [uuid]);
  }

  static Future<int> update(String table, Map<String, dynamic> item) async {
    return await _db!.update(
      table,
      item,
      where: "uuid = ?",
      whereArgs: [item['uuid']],
    );
  }
}
