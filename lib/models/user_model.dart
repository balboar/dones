class User {
  final String? user;
  final String? token;

  User({required this.user, this.token});
}
