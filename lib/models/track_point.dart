import 'package:latlong2/latlong.dart';

class TrackPoint {
  double? altitude;
  double elapsedTime;
  double elapsedDistance;
  double speed;
  LatLng position;

  TrackPoint(this.elapsedTime, this.altitude, this.elapsedDistance, this.speed,
      this.position);
}
