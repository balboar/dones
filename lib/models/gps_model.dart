import 'dart:convert';

import 'package:latlong2/latlong.dart';

class GpsPoint {
  static String table = 'posiciones';
  final int id;
  final double latitude;
  final double longitude;
  final double accuracy;
  double elevation;
  final double speed;
  final double heading;
  final double date;
  final String ruta_id;
  final LatLng posicion;
  final int battery;
  double distanciaInicio;
  GpsPoint({
    required this.id,
    required this.latitude,
    required this.longitude,
    required this.accuracy,
    required this.elevation,
    required this.speed,
    required this.heading,
    required this.date,
    required this.ruta_id,
    required this.posicion,
    required this.battery,
    this.distanciaInicio = 0.0,
  });

  GpsPoint copyWith({
    int? id,
    double? latitude,
    double? longitude,
    double? accuracy,
    double? elevation,
    double? speed,
    double? heading,
    double? date,
    String? ruta_id,
    LatLng? posicion,
    int? battery,
  }) {
    return GpsPoint(
      id: id ?? this.id,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      accuracy: accuracy ?? this.accuracy,
      elevation: elevation ?? this.elevation,
      speed: speed ?? this.speed,
      heading: heading ?? this.heading,
      date: date ?? this.date,
      ruta_id: ruta_id ?? this.ruta_id,
      posicion: posicion ?? this.posicion,
      battery: battery ?? this.battery,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'latitude': latitude,
      'longitude': longitude,
      'accuracy': accuracy,
      'elevation': elevation,
      'speed': speed,
      'heading': heading,
      'date': date,
      'ruta_id': ruta_id,
      'battery': battery,
    };
  }

  factory GpsPoint.fromMap(Map<String, dynamic> map) {
    return GpsPoint(
      id: map['id']?.toInt() ?? 0,
      latitude: map['latitude']?.toDouble() ?? 0.0,
      longitude: map['longitude']?.toDouble() ?? 0.0,
      accuracy: map['accuracy']?.toDouble() ?? 0.0,
      elevation: map['elevation']?.toDouble() ?? 0.0,
      speed: map['speed']?.toDouble() ?? 0.0,
      heading: map['heading']?.toDouble() ?? 0.0,
      date: map['date']?.toDouble() ?? 0.0,
      ruta_id: map['ruta_id'] ?? '',
      posicion:
          LatLng(map['latitude']?.toDouble(), map['longitude']?.toDouble()),
      battery: map['battery']?.toInt() ?? 0,
    );
  }

  factory GpsPoint.fromDBMap(Map<String, dynamic> map) {
    return GpsPoint(
      id: map['id']?.toInt() ?? 0,
      latitude: map['latitude']?.toDouble() ?? 0.0,
      longitude: map['longitude']?.toDouble() ?? 0.0,
      accuracy: map['accuracy']?.toDouble() ?? 0.0,
      elevation: map['elevation']?.toDouble() ?? 0.0,
      speed: map['speed']?.toDouble() * 3.6 ?? 0.0,
      heading: map['heading']?.toDouble() ?? 0.0,
      date: map['date']?.toDouble() ?? 0.0,
      ruta_id: map['ruta_id'] ?? '',
      posicion:
          LatLng(map['latitude']?.toDouble(), map['longitude']?.toDouble()),
      battery: map['battery']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory GpsPoint.fromJson(String source) =>
      GpsPoint.fromMap(json.decode(source));

  @override
  String toString() {
    return 'GpsPoint(id: $id, latitude: $latitude, longitude: $longitude, accuracy: $accuracy, elevation: $elevation, speed: $speed, heading: $heading, date: $date, ruta_id: $ruta_id, posicion: $posicion, battery: $battery)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GpsPoint &&
        other.id == id &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.accuracy == accuracy &&
        other.elevation == elevation &&
        other.speed == speed &&
        other.heading == heading &&
        other.date == date &&
        other.ruta_id == ruta_id &&
        other.posicion == posicion &&
        other.battery == battery;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        latitude.hashCode ^
        longitude.hashCode ^
        accuracy.hashCode ^
        elevation.hashCode ^
        speed.hashCode ^
        heading.hashCode ^
        date.hashCode ^
        ruta_id.hashCode ^
        posicion.hashCode ^
        battery.hashCode;
  }
}
