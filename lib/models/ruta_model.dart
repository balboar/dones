import 'dart:convert';

import 'package:dones/utils/constants.dart';

class Ruta {
  static String table = 'rutas';
  String uuid;
  int date;
  Sport sport;
  String? ruta;
  bool finished;
  Ruta({
    required this.uuid,
    required this.date,
    required this.sport,
    required this.finished,
    this.ruta,
  });

  Ruta copyWith({
    String? uuid,
    int? date,
    Sport? sport,
    String? ruta,
    bool? finished,
  }) {
    return Ruta(
        uuid: uuid ?? this.uuid,
        date: date ?? this.date,
        sport: sport ?? this.sport,
        ruta: ruta ?? this.ruta,
        finished: finished ?? this.finished);
  }

  Map<String, dynamic> toMap() {
    return {
      'uuid': uuid,
      'date': date,
      'sport': sport.value,
      'ruta': ruta,
      'finished': finished ? 1 : 0
    };
  }

  factory Ruta.fromMap(Map<String, dynamic> map) {
    return Ruta(
        uuid: map['uuid'] ?? '',
        date: map['date']?.toInt() ?? 0,
        sport: SportHelper.asignar(map['sport']),
        ruta: map['ruta'],
        finished: map['finished'] == 1 ? true : false);
  }

  factory Ruta.fromApiMap(Map<String, dynamic> map) {
    return Ruta(
        uuid: map['uuid'] ?? '',
        date: map['date']?.toInt() ?? 0,
        sport: SportHelper.asignar(map['sport']),
        ruta: map['gpxRoute'],
        finished: map['finished'] == 1 ? true : false);
  }

  String toJson() => json.encode(toMap());

  factory Ruta.fromJson(String source) => Ruta.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Ruta(uuid: $uuid, timestamp: $date, sport: $sport, ruta: $ruta)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Ruta &&
        other.uuid == uuid &&
        other.date == date &&
        other.sport == sport &&
        other.ruta == ruta &&
        other.finished == finished;
  }

  @override
  int get hashCode {
    return uuid.hashCode ^
        date.hashCode ^
        sport.hashCode ^
        ruta.hashCode ^
        finished.hashCode;
  }
}
