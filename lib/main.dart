import 'dart:io';

import 'package:dones/controllers/dones_controller.dart';
import 'package:dones/controllers/theme_controller.dart';
import 'package:dones/controllers/user_controller.dart';
import 'package:dones/database/database2.dart';
import 'package:dones/ui/pages/ajustes_page.dart';
import 'package:dones/ui/pages/auth.dart';
import 'package:dones/ui/pages/mainPage/cubit/socket_cubit.dart';
import 'package:dones/ui/pages/mainPage/mainPage.dart';
import 'package:dones/utils/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FMTCObjectBoxBackend().initialise();
  await const FMTCStore('mapStore').manage.create();

  if (Platform.isAndroid) await Firebase.initializeApp();
  //await databaseFactory.setLogLevel(sqfliteLogLevelVerbose);
  await DB.init();

  runApp(ChangeNotifierProvider(
    create: (context) => ThemeService(),
    child: const MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  final UserController _usermodel = UserController();

  bool _isAuthenticated = false;

  @override
  void initState() {
    _usermodel.autoAuthenticate(context);
    _usermodel.userSubject.listen((bool isAuthenticated) {
      setState(() {
        _isAuthenticated = isAuthenticated;
      });
    });
    SharedPreferences.getInstance().then((prefs) {
      var darkMode = prefs.getBool(
            'themeModeDart',
          ) ??
          true;
      Provider.of<ThemeService>(context, listen: false).setTheme(darkMode);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          ChangeNotifierProvider<UserController>.value(value: _usermodel),
          ChangeNotifierProvider<DonesController>(
            create: (_) => DonesController(),
          ),
          BlocProvider<SocketCubit>(
              create: (BuildContext context) => SocketCubit(
                  donesControllerRepo:
                      Provider.of<DonesController>(context, listen: false))
                ..connect())
        ],
        child: Consumer<ThemeService>(builder: (context, themeService, child) {
          SystemChrome.setSystemUIOverlayStyle(themeService.isDarkModeOn
              ? SystemUiOverlayStyle.light.copyWith(
                  statusBarColor: Colors.transparent,
                )
              : SystemUiOverlayStyle.dark);
          return MaterialApp(
            themeMode:
                themeService.isDarkModeOn ? ThemeMode.dark : ThemeMode.light,
            title: 'Dones',
            theme: AppTheme.lightTheme,
            darkTheme: AppTheme.darkTheme,
            routes: {
              '/': (BuildContext context) =>
                  !_isAuthenticated ? AuthPage() : const Mainpage(),
              '/ajustes': (BuildContext context) => AjustesPage(),
            },
          );
        }));
  }
}
