import 'dart:io';
import 'package:dones/networking/CustomException.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

String user = const String.fromEnvironment('user');
String pass = const String.fromEnvironment('password');
String auth = 'Basic ${base64Encode(utf8.encode('$user:$pass'))}';
String url = const String.fromEnvironment('apiUrl');

Map<String, String> _jsonHeaders = {
  // "content-type": "application/json",
  'authorization': auth
};

class ApiProvider {
  Future<String> getFilePath() async {
    Directory appDocumentsDirectory =
        await getApplicationSupportDirectory(); // 1
    String appDocumentsPath = appDocumentsDirectory.path; // 2
    await Directory('$appDocumentsPath/dones/').create();

    return appDocumentsPath;
  }

  Future<dynamic> _get(String url) async {
    var responseJson;
    try {
      final response = await http.get(Uri.parse(url), headers: _jsonHeaders);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('Servidor no contesta');
    } catch (e) {
      throw FetchDataException(e.toString());
    }
    return responseJson;
  }

  Future<dynamic> deleteRuta(String uuid) async {
    var responseJson;
    try {
      final response = await http.delete(Uri.parse('$url/activity/$uuid'),
          headers: _jsonHeaders);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('Servidor no contesta');
    } catch (e) {
      throw FetchDataException(e.toString());
    }
    return responseJson;
  }

  Future<dynamic> getRoute(String file) async {
    return await _get(url + file);
  }

  Future<dynamic> getNuevasPosiciones(int timestamp) async {
    return await _get('$url/position/$timestamp');
  }
}

dynamic _returnResponse(http.Response response) {
  switch (response.statusCode) {
    case 200:
      dynamic responseJson = null;
      try {
        responseJson = json.decode(response.body.toString());
      } on FormatException {
        responseJson = json.decode(json.encode(response.body));
      }

      return responseJson;
    case 201:
      var responseJson = json.decode(response.body.toString());
      return responseJson;
    case 400:
      throw BadRequestException(response.body.toString());
    case 401:
    case 403:
      throw UnauthorisedException(response.body.toString());
    case 500:
    default:
      throw FetchDataException(
          'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
  }
}
