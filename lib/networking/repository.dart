import 'dart:io';

import 'package:dones/networking/ApiProvider.dart';
import 'package:dones/networking/apiResponse.dart';
import 'package:path_provider/path_provider.dart';

class Repository {
  final ApiProvider _provider = ApiProvider();

  Future<String> getRoutesPath() async {
    Directory appDocumentsDirectory =
        await getApplicationSupportDirectory(); // 1
    String appDocumentsPath = appDocumentsDirectory.path; // 2

    await Directory('$appDocumentsPath/routes/').create();

    return appDocumentsPath;
  }

  Future<bool> processRoute(String? filename, String content) async {
    String fileName = await getRoutesPath();

    fileName = '$fileName/routes/$filename';
    File file = File(fileName);
    await file.writeAsString(
      content,
    );
    return Future.value(true);
  }

  Future<dynamic> getNuevasPosiciones(int timestamp) async {
    return await _provider.getNuevasPosiciones(timestamp);
  }

  Future<dynamic> deleteRuta(String uuid) async {
    return await _provider.deleteRuta(uuid);
  }

  Future<Response<Map<String, dynamic>>> fetchDonesRoute(String? file) async {
    try {
      final response = await _provider.getRoute('/track/$file');

      await processRoute(file,response);

      return Response.completed({'message': 'ok'});
    } catch (e) {
      return Response.error(e.toString());
    }
  }
}
